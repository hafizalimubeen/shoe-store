<?php include('includes/views/session.php');?>
<!DOCTYPE html>
<html>
  <!--head-->
  <?php include('includes/html-parts/head.php');?>
  <!--head -->
  <body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
    <!-- header -->
      <?php include('includes/views/header.php');?>
      <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>        
              <?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?>
              </p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span> 
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->

          </ul><!-- /.sidebar-menu --> 
          <ul class="sidebar-menu">
            <li class="active"><a href="add_inventory.php"><i class="fa fa-folder"></i>Add Inventory<i class="fa fa-angle-left pull-right"></i></a></li>
            <li class=""><a href="detail_inventory.php"><i class="fa fa-folder"></i>Purchase Order<i class="fa fa-angle-left pull-right"></i></a></li>
          </ul><!-- /.sidebar-menu --> 
        </section> 
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>            
              <i class="ion ion-bag" style="margin-left: 50px;color:#57bddb;"></i>Inventory Management
          </h1>                     
        </section> 


      <div class=" col-md-12" style="background: white;padding-top: 30px;margin-top:25px;">
        <div class="col-md-offset-2 col-sm-8">
            
              <!--  -->
              <!-- User Registration   -->
              <!--  -->
              <div id="purchase_order" class="box box-primary">                  
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Please Select Order Detail</b></h3>
                </div>
                <div id="order_detail" class="form-horizontal">
                  <div class="box-body">
                      
                        <div class="form-group">                        
                            <label class="col-sm-4 control-label" for="username">Select a Supplier</label>
                            <div class="col-sm-8">
                              <select class="form-control" id="order_supplier">
                                  <option value="" disabled selected>Please Select a Supplier</option>
                              </select> 
                            </div>                     
                        </div>
                      
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="exampleInputPassword1">Enter Number of Products to Add</label> <div class="col-sm-8">                     
                              <input type="text" class="form-control" id="order_quan" placeholder="Enter Quantity of Order"/>
                              <p class="small">(Add number of products to be added, individually. If you have same product in more than one quantity, add only 1 quantity.)</p>
                            </div>

                        </div>                                                   
                      
                        <div class="form-group">
                            <label style="color:#57bddb">Your are Logged in As: </label>
                            <label><?php if(isset($_SESSION['usertype'])){ echo " ".$_SESSION['usertype'];}?></label>
                        </div>   
                       
                  </div>
                    
                  <div id="order_error_message" class="validatr-message" style="color: rgb(240, 68, 77); border: 1px solid rgb(228, 166, 175); padding:0px 6px; border-radius: 0px; position: relative; left: 19px; top: 0px; background-color: rgb(255, 203, 203);">
                   </div>  
                  <div class="box-footer">
                    <button id="submit_order_detail" class="btn btn-primary btn-block">Add</button>
                  </div>
                </div>
              </div>
              <!--  -->
              <!-- Order DETAILS end -->
              <!-- -->


              <!-- Adding Purchases -->
              
              <div id="adding_order" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Please Add Order Details</b></h3><br/>
                  
                  <label style="color:#57bddb">Added:</label>
                  <label id="done">0</label>
                  
                  <label style="margin-left:20px;color:#57bddb">Left:</label>
                  <label id="remaining" >0</label>
                  
                </div>
                <div id="order" class="form-horizontal">
                  <div class="box-body">                                              
                      
                        <div class="form-group">                        
                            <label class="col-sm-4 control-label" for="order_maker">Select a Maker</label>
                            <div class="col-sm-8">
                              <select class="form-control" id="order_maker">
                                  <option value="" disabled selected>Please Select a Maker</option>
                              </select>
                            </div>                      
                        </div>
                        
                        <div class="form-group">                        
                            <label class="col-sm-4 control-label" for="order_detail_type">Select a Type</label>
                            <div class="col-sm-8">
                              <select class="form-control" id="order_detail_type">
                                  <option value="" disabled selected>Please Select a Type</option>
                              </select> 
                            </div>                     
                        </div>

                        <div class="form-group">                        
                            <label class="col-sm-4 control-label" for="order_detail_article">Select an Article Title</label>
                            <div class="col-sm-8">
                              <select class="form-control" id="order_detail_article">
                                  <option value="" disabled selected>Please Select an Article</option>
                              </select>  
                            </div>                    
                        </div>

                        <div id="shoe-specs">
                          <div class="form-group">                        
                              <label class="col-sm-4 control-label" for="order_detail_article">Article Number</label>
                              <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="order_detail_article_no" />
                              </div>                      
                          </div>

                          <div class="form-group">                        
                              <label class="col-sm-4 control-label" for="order_detail_article">Shoe Size</label>
                              <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="order_detail_size" />
                              </div>                      
                          </div>
                          
                          <div class="form-group">                        
                              <label class="col-sm-4 control-label" for="order_detail_color">Shoe Color</label>
                              <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="order_detail_color" /> 
                              </div>                  
                          </div>

                          <div class="form-group">                        
                              <label class="col-sm-4 control-label" for="username">Shoe Wearing</label>
                              <div class="col-sm-8">
                                <input type="text" readonly class="form-control" id="order_detail_gender" />
                              </div>                     
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="another_name">Enter extra title</label>
                            <div class="col-sm-8">                      
                              <input type="text" class="form-control" id="another_name" placeholder="Enter another name if any"/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="order_price">Enter Purchasing Price</label>
                            <div class="col-sm-8">                      
                              <input type="text" class="form-control" id="order_price" placeholder="Enter Price of Shoes"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="order_quantity">Enter Shoes Quantity</label>
                            <div class="col-sm-8">                      
                              <input type="number" min="1" max="100" class="form-control" id="order_quantity" placeholder="Enter Quantity of Shoes"/>
                            </div>
                        </div>
                                              
                  </div>
                    
                  <div id="order_detail_error_message" class="validatr-message" style="color: rgb(240, 68, 77); border: 1px solid rgb(228, 166, 175); padding: 2px 6px; border-radius: 0px; position: relative; left: 19px; top: 0px; background-color: rgb(255, 203, 203);">
                   </div>  
                  <div class="box-footer">
                    <div class="col-sm-offset-2 col-sm-4">
                      <button id="submit_product_detail" class="btn btn-primary btn-block">Next</button>
                    </div>
                    <div class="col-sm-4">
                      <button id="done_order" class="btn btn-primary btn-block" style="margin-left:50px">Finish</button>                    
                    </div>
                  </div>
                </div>
              </div>              
              
              <!-- Ended Adding Purchases -->    
              </div> 
              </div>                                                         




      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include('includes/views/footer.php');?>

      <!-- Control Sidebar -->
      <?php include('includes/views/rightbar.php');?>
      
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
        <!-- <script>
             var map;
             function initMap(){                
                    map = new google.maps.Map(document.getElementById('world-map'), {
                    center: {lat: 30.774505540304002, lng: 72.43481934070587},
                    zoom: 16                   
                    });        
                   var marker=new google.maps.Marker({
                        position:{lat: 30.774505540304002,lng: 72.43481934070587},
                        map:map,
                   });              
             }        
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsogWjl6FoMi73yLHQJcPe5E7yDf7xBng&callback=initMap"
    async defer></script> -->
  </body>
</html>