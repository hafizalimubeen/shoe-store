<!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jQueryUI/jquery-ui.min.js"></script> 
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    
        <!-- iCheck Boxes-->
    <script src="plugins/iCheck/icheck.min.js"></script>
    
    <!-- daterangepicker -->
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
<!--     <script src="plugins/fullcalendar/fullcalendar.min.js"></script> -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/datatables/dataTables.buttons.min.js"></script>
<!--     <script src="plugins/datatables/dataTables.editor.min.js"></script> -->
    <script src="plugins/datatables/dataTables.select.min.js"></script>
    <!-- Semantic UI for search along dropdown -->
    <script src="plugins/semanticUI/dist/semantic.js"></script>
    <!-- Choosen Plugin -->
    <script src="plugins/chosen/chosen.jquery.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- team js files -->
    <script src="dist/js/local-m.js"></script>
    <script src="dist/js/local-admin.js"></script>
    <script src="dist/js/local-admin-update.js"></script>
    <script src="dist/js/local-inventory.js"></script>
    <script src="dist/js/local-sales.js"></script>
    <script src="dist/js/local-reports.js"></script>   