<?php 
$Notification_SUC="Success";
$Notification_INFO="Info";
$Notification_INFO_W="Info_W";
$Notification_FAIL="Fail";

// Common Functions
	function insertRecord($dbc,$table,$cols,$values){
		$q="";
		$q.="INSERT INTO ".$table." ( ".$cols[0];
		for($i=1;$i<count($cols);$i++){
			$q.=" , ".$cols[$i];
		}
		$q.=" ) VALUES ( '".$values[0]."'";
		$needle="f~~";
		for($i=1;$i<count($values);$i++){
			$q.=" ,";
			if(substr($values[$i],0,strlen($needle))===$needle){
				$q.=" ".substr($values[$i],strlen($needle))." ";
			}
			else 
			$q.=" '".$values[$i]."' ";
		
		}
		$q.=" ) ";
		// echo $q;
		return mysqli_query($dbc,$q);
	}
	function insertRecordOrUpdate($dbc,$table,$cols,$values){
	$q="";
	$strU=" ".$cols[0]."='".$values[0]."' ";
	$q.="INSERT INTO ".$table." ( ".$cols[0];
	$needle="f~~";
	for($i=1;$i<count($cols);$i++){
		$q.=" , ".$cols[$i];
		$v="";
		if(substr($values[$i],0,strlen($needle))===$needle){
			$v.=" ".substr($values[$i],strlen($needle))." ";
		}
		else $v.=" '".$values[$i]."' ";
		$strU.=" , ".$cols[$i]."=".$v;
	}
	$q.=" ) VALUES ( '".$values[0]."'";
	
	for($i=1;$i<count($values);$i++){
		$q.=" ,";
		if(substr($values[$i],0,strlen($needle))===$needle){
			$q.=" ".substr($values[$i],strlen($needle))." ";
		}
		else 
		$q.=" '".$values[$i]."' ";
	
	}
	$q.=" ) ON DUPLICATE KEY UPDATE ".$strU;
	//echo $q;
	return mysqli_query($dbc,$q);
}

function updateRecord($dbc,$table,$cols,$values,$conCols,$conOp,$conVal){
	$q="UPDATE $table ";
	$q.=" SET ".$cols[0]."='".$values[0]."' ";
	for($i=1;$i<count($cols);$i++){
		$q.=" , ".$cols[$i]."='".$values[$i]."' ";
	}
	$q.=" WHERE ".$conCols[0]."='".$conVal[0]."' ";
	for($i=1;$i<count($conCols);$i++){
		$q.=$conOp[$i-1]." ".$conCols[$i]."='".$conVal[$i]."' ";
	}
	return mysqli_query($dbc,$q);
}

function deleteRecord($dbc,$table,$cols,$values,$conCols,$conOp,$conVal){
	$q="DELETE FROM $table ";
	$q.=" WHERE ".$conCols[0]."='".$conVal[0]."' ";
	for($i=1;$i<count($conCols);$i++){
		$q.=$conOp[$i-1]." ".$conCols[$i]."='".$conVal[$i]."' ";
	}
	return mysqli_query($dbc,$q);
}

function send_mail($host,$receiver,$sender,$password,$subject,$Html)
{      
	require_once '../functions/mailer/PHPMailerAutoload.php';
	$mail = new PHPMailer;
	$mail->SMTPDebug = 0;                               // Enable verbose debug output
	$mail->Debugoutput = 'html';
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $host;
	$mail->Port =  80;
	$mail->SMTPAuth = true;
	$mail->Username = $sender;
	$mail->Password = $password;                                 
	$mail->setFrom($sender, 'Testing Account');
	$mail->addAddress($receiver);               // Name is optional
	$mail->addReplyTo($sender, 'Testing Account');
	$mail->isHTML(true);                                  // Set email format to HTML
	$mail->Subject = $subject;
	$mail->Body    = $Html;
	$mail->AltBody = ' ';
	if(!$mail->send()) {
		echo "Email not sent.";
	}
	else
	{     
		echo "Email has sent to your account.";
	}
}

function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }
     
    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using FUNCTION (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }
    else {
         // Return array
         return $d;
    }
}
?>
