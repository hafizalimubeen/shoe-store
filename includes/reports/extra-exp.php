<?php 
include '../db/dbconfig.php';
$userid=$_GET['user_id'];
$to=$_REQUEST['date-to'];
$from=$_REQUEST['date-from'];
if(strlen($to)>0){
  $to=explode("-",$to);
  if(count($to)==1)$to=explode("/",$to);
  $to=mktime(0,0,0,$to[1],$to[2],$to[0]);
  $to=" AND expense_date<=$to";
}
if(strlen($from)>0){
  $from=explode("-",$from);
  if(count($from)==1)$from=explode("/",$from);
  $from=mktime(0,0,0,$from[1],$from[2],$from[0]);
  $from=" AND expense_date>=$from";
}
$where=" WHERE user_id = '".$userid."'  $to $from
";


$q="SELECT *
FROM `extra_expense`
 $where";
// echo $q;
$result=mysqli_query($conn,$q);
?>
<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Expense Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="extra-expense" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>Amount</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i=0; 
                      $total=0;
                      while($row=mysqli_fetch_assoc($result)){
                        $i++;
                      echo "<tr>
                              <td>{$i}</td>                              
                              <td>{$row['expense_dtl']}</td>
                              <td>".date('m/d/Y h:m:sa', $row['expense_date'])."</td>
                              <td>{$row['expense_amnt']}</td>                               
                            </tr>    ";
                            $total+=$row['expense_amnt'];
                            }
                      ?>     
                    </tbody>
                    <tfoot>
                      <?php 
                      echo "<tr>
                              <td></td>
                              <td></td>
                              <td>Total</td>
                              <td><b>$total</b></td>
                            </tr>    ";
                      ?>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->