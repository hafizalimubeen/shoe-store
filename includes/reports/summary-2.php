<?php 
include '../db/dbconfig.php';
$article=$_REQUEST['article'];
$gender=$_REQUEST['gender'];
$color=$_REQUEST['color'];
$size=$_REQUEST['size'];
$to=$_REQUEST['date-to'];
$from=$_REQUEST['date-from'];
$userid=$_GET['user_id'];
if(strlen($to)>0){
  $to=explode("-",$to);
  //if(count($to)==1)$to=explode("/",$to);
  $to=mktime(0,0,0,$to[1],$to[2],$to[0]);
  $to=" AND date<=$to";
}

if(strlen($from)>0){
  $from=explode("-",$from);
  //if(count($from)==1)$from=explode("/",$from);
  $from=mktime(0,0,0,$from[1],$from[2],$from[0]);
  $from=" AND date>=$from";
}

$where="Where a.article_id like '$article%' AND p.user_id = '".$userid."'
 $to $from
";

$q="SELECT a.article_title,count(*) count, count(NULLIF(TRIM(p.status), '')) sold
FROM product as p
inner join maker on maker.maker_id = p.maker_id
inner join `type` as t on t.type_id = p.type_id
inner join article as a on a.article_id = p.article_id
inner join purchase_order_detail pd on pd.product_id=p.product_id
inner join purchase_order po on po.po_id=pd.po_Id 
$where

group by a.article_id
ORDER BY a.article_title ASC
";
// echo $q;
$result=mysqli_query($conn,$q);
?>
<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Article Wise Summary</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="summary-2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr</th>
                        <th>Article</th>
                        <th>Count</th>
                        <th>Sold</th>
                        <th>Not Sold</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i=0;
                      $count=0;
                      $sold=0; while($row=mysqli_fetch_assoc($result)){
                        $i++;
                      echo "<tr>
                              <td>$i</td>                              
                              <td>{$row['article_title']}</td>
                              <td>{$row['count']}</td>
                               <td>{$row['sold']}</td>                               
                               <td>".($row['count']-$row['sold'])."</td>                         
                            </tr>    ";
                            $count+=$row['count'];
                            $sold+=$row['sold'];
                            }
                      ?>
                    </tbody>
                    <tfoot>
                      <?php 
                      echo "<tr>
                              <td></td>                              
                              <td>Total</td>
                              <td>$count</td>
                               <td>$sold</td>                               
                               <td>".($count-$sold)."</td>                               
                            </tr>    ";
                      ?>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            <?php         
$q="SELECT p.gender_id,count(*) count,count(NULLIF(TRIM(p.status), '')) sold
FROM product as p
inner join maker on maker.maker_id = p.maker_id
inner join `type` as t on t.type_id = p.type_id
inner join article as a on a.article_id = p.article_id
inner join purchase_order_detail pd on pd.product_id=p.product_id
inner join purchase_order po on po.po_id=pd.po_Id 
$where
 
group by p.gender_id 
";
// echo $q;
$result=mysqli_query($conn,$q);
?>
<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Gender Wise Summary</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="mk-summary-2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr</th>
                        <th>Make</th>
                        <th>Count</th>
                        <th>Sold</th>
                        <th>Not Sold</th>                        
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $count=0;
                      $sold=0;
                      $i=0; while($row=mysqli_fetch_assoc($result)){
                        $i++;
                      echo "<tr>
                              <td>$i</td>                              
                              <td>{$row['gender_id']}</td>
                              <td>{$row['count']}</td>
                               <td>{$row['sold']}</td>                               
                               <td>".($row['count']-$row['sold'])."</td>
                            </tr>    ";
                            $count+=$row['count'];
                            $sold+=$row['sold'];
                            }
                      ?>
                    </tbody>
                    <tfoot>
                      <?php 
                      echo "<tr>
                              <td></td>                              
                              <td>Total</td>
                              <td>$count</td>
                               <td>$sold</td>                               
                               <td>".($count-$sold)."</td>
                            </tr>    ";
                      ?>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->