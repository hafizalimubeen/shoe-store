<?php 
include '../db/dbconfig.php';
$article=$_REQUEST['article'];
$gender=$_REQUEST['gender'];
$color=$_REQUEST['color'];
$size=$_REQUEST['size'];
$to=$_REQUEST['date-to'];
$from=$_REQUEST['date-from'];
$userid=$_GET['user_id'];
if(strlen($to)>0){
  $to=explode("-",$to);
  //if(count($to)==1)$to=explode("/",$to);
  $to=mktime(0,0,0,$to[1],$to[2],$to[0]);
  $to=" AND sales_time<=$to";
}

if(strlen($from)>0){
  $from=explode("-",$from);
  //if(count($from)==1)$from=explode("/",$from);
  $from=mktime(0,0,0,$from[1],$from[2],$from[0]);
  $from=" AND sales_time>=$from";
}

$where=" WHERE length(status)>0 AND  a.article_id like '$article%' AND p.user_id = '".$userid."'
 $to $from
";




$q="SELECT a.article_title,count(*) count,sum(purch_price) p_total,sum(sales_price) s_total
FROM product as p
inner join maker on maker.maker_id = p.maker_id
inner join `type` as t on t.type_id = p.type_id
inner join article as a on a.article_id = p.article_id
inner join sales_order_detail sd on sd.product_id=p.product_id
inner join sales_order so on so.so_id=sd.so_id
 $where
 
GROUP BY a.article_id
ORDER BY a.article_title ASC 
";
//echo $q;
$result=mysqli_query($conn,$q);
?>
<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Article Wise Summary</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="summary" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr</th>
                        <th>Article</th>
                        <th>Count</th>
                        <th>Sale Price</th>
                        <th>Purchase Price</th>
                        <th>Profit/ Loss</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i=0;
                      $sp=0;
                      $pp=0;
                      $count=0; while($row=mysqli_fetch_assoc($result)){
                        $i++;
                      echo "<tr>
                              <td>$i</td>
                              
                              <td>{$row['article_title']}</td>
                              <td>{$row['count']}</td>
                               <td>{$row['s_total']}</td>
                               <td>{$row['p_total']}</td>
                               <td>".($row['s_total']-$row['p_total'])."</td>
                               
                               
                               
                            </tr>    ";
                            $sp+=$row['s_total'];
                            $pp+=$row['p_total'];
                            $count+=$row['count'];
                            }
                      ?>
                    </tbody>
                    <tfoot>
                      <?php 
                     echo "<tr>
                              <td></td>
                              <td>Total</td>
                              <td>$count</td>
                               
                               <td>$sp</td>
                               <td>$pp</td>
                               <td>".($sp-$pp)."</td>
                               
                               
                               
                            </tr>    ";
                     ?>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            <?php   
              
$q="SELECT p.gender_id,count(*) count,sum(purch_price) p_total,sum(sales_price) s_total
FROM product as p
inner join maker on maker.maker_id = p.maker_id
inner join `type` as t on t.type_id = p.type_id
inner join article as a on a.article_id = p.article_id
inner join sales_order_detail sd on sd.product_id=p.product_id
inner join sales_order so on so.so_id=sd.so_id
 $where
 
 group by p.gender_id 
";
//echo $q;
$result=mysqli_query($conn,$q);
?>
<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Gender Wise Summary</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="mk-summary" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr</th>
                        <th>Gender</th>
                        <th>Count</th>
                        <th>Sale Price</th>
                        <th>Purchase Price</th>
                        <th>Profit/ Loss</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i=0;
                      
                      $sp=0;
                      $pp=0;
                      $count=0;
                       while($row=mysqli_fetch_assoc($result)){
                        $i++;
                      echo "<tr>
                              <td>$i</td>
                              
                              <td>{$row['gender_id']}</td>
                              <td>{$row['count']}</td>
                               <td>{$row['s_total']}</td>
                               <td>{$row['p_total']}</td>
                               <td>".($row['s_total']-$row['p_total'])."</td>
                               
                               
                               
                            </tr>    ";
                            $sp+=$row['s_total'];
                            $pp+=$row['p_total'];
                            $count+=$row['count'];
                            }
                      ?>
                    </tbody>
                    <tfoot>
                       <?php 
                     echo "<tr>
                              <td></td>
                              <td>Total</td>
                              <td>$count</td>
                               
                               <td>$sp</td>
                               <td>$pp</td>
                               <td>".($sp-$pp)."</td>
                               
                               
                               
                            </tr>    ";
                     ?>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->