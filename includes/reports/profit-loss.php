<?php 
include '../db/dbconfig.php';
$article=$_REQUEST['article'];
$gender=$_REQUEST['gender'];
$color=$_REQUEST['color'];
$size=$_REQUEST['size'];
$to=$_REQUEST['date-to'];
$from=$_REQUEST['date-from'];
$userid=$_GET['user_id'];
if(strlen($to)>0){
  $to=explode("-",$to);
  //if(count($to)==1)$to=explode("/",$to);
  $to=mktime(0,0,0,$to[1],$to[2],$to[0]);
  $to=" AND sales_time<=$to";
}

if(strlen($from)>0){
  $from=explode("-",$from);
  //if(count($from)==1)$from=explode("/",$from);
  $from=mktime(0,0,0,$from[1],$from[2],$from[0]);
  $from=" AND sales_time>=$from";
}

$where=" WHERE length(status)>0 AND  a.article_id like '$article%' AND p.user_id = '".$userid."'
 $to $from
";

$to_exp=$_REQUEST['date-to'];
$from_exp=$_REQUEST['date-from'];
if(strlen($to_exp)>0){
  $to_exp=explode("-",$to_exp);
  if(count($to_exp)==1)$to_exp=explode("/",$to_exp);
  $to_exp=mktime(0,0,0,$to_exp[1],$to_exp[2],$to_exp[0]);
  $to_exp=" AND expense_date<=$to_exp";
}
if(strlen($from_exp)>0){
  $from_exp=explode("-",$from_exp);
  if(count($from_exp)==1)$from_exp=explode("/",$from_exp);
  $from_exp=mktime(0,0,0,$from_exp[1],$from_exp[2],$from_exp[0]);
  $from_exp=" AND expense_date>=$from_exp";
}
$where_exp=" WHERE `user_id` = '$userid' $to_exp $from_exp
";


$q="SELECT *
FROM product as p
inner join maker on maker.maker_id = p.maker_id
inner join `type` as t on t.type_id = p.type_id
inner join article as a on a.article_id = p.article_id
inner join sales_order_detail sd on sd.product_id=p.product_id
inner join sales_order so on so.so_id=sd.so_id
$where 

";
// echo $q;
$result=mysqli_query($conn,$q);
?>
<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Profit Loss Statement</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="profit-loss" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr</th>
                        <th>Article No</th>
                        <th>Gender</th>
                        <th>Color</th>
                        <th>Size</th>
                        <th>Sale Price</th>
                        <th>Purchase Price</th>
                        <th>Profit/ Loss</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i=0;
                      $sp=0;
                      $pp=0; while($row=mysqli_fetch_assoc($result)){
                        $i++;
                      echo "<tr>
                              <td>{$row['product_id']}</td>                              
                              <td>{$row['article_no']}</td>
                              <td>{$row['gender_id']}</td>
                               <td>{$row['color_id']}</td>
                               <td>{$row['size_id']}</td>
                               <td>{$row['sales_price']}</td>
                               <td>{$row['purch_price']}</td>
                               <td>".($row['sales_price']-$row['purch_price'])."</td>                              
                            </tr>    ";
                            $sp+=$row['sales_price'];
                            $pp+=$row['purch_price'];
                            }
                      ?>
                    </tbody>
                    <tfoot>
                     <?php 
                     echo "<tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                               <td>Gross Profit</td>
                               <td>$sp</td>
                               <td>$pp</td>";
                               if($sp>$pp){
                                echo "<td style=\"color:green;border-top: 2px solid;\"><b>".($sp-$pp)."</b></td>";
                               }else if($sp<$pp){
                                echo "<td style=\"color:red;border-top: 2px solid;\"><b>".($sp-$pp)."</b></td>";
                               }else{
                                echo "<td><b>".($sp-$pp)."</b></td>";  
                               }                               
                            echo "</tr>    ";
                            $q="SELECT * FROM `extra_expense` $where_exp";
                            $result=mysqli_query($conn,$q);
                            $total12 =0;
                            while($row=mysqli_fetch_assoc($result)){
                              $total12+=$row['expense_amnt'];
                            }
                            echo "<tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td>Total Expense</td>
                              <td></td>
                              <td></td>
                              <td style=\"border-bottom: 2px solid;\"><b>$total12</b></td>
                            </tr>    ";
                            echo "<tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td>Net Income</td>
                              <td></td>
                              <td></td>";
                              if(($sp-$pp)>$total12){
                                echo "<td style=\"color:green;\"><b>".(($sp-$pp)-$total12)."</b></td>";
                               }else if(($sp-$pp)<$total12){
                                echo "<td style=\"color:red;\"><b>".(($sp-$pp)-$total12)."</b></td>";
                               }else{
                                echo "<td><b>".(($sp-$pp)-$total12)."</b></td>";  
                               }
                            echo "</tr>    ";
                     ?>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->