<?php 
include("../db/dbconfig.php");
include("../db/password_hash.php");
include("../functions/functions.php");

$date = new DateTime();            
$timestamp = $date->getTimestamp();

////////////////////Supplier informaion Addition, Updation or deletion Section//////////////////////////
//Ajax request for adding new Supplier
if(isset($_POST['oper']) && $_POST['oper'] == "addingSupplier")
{
	$sup_id = md5(uniqid(rand()));
	$sup_name = mysqli_real_escape_string($conn, $_POST['sup_name']);
	$sup_phn = mysqli_real_escape_string($conn, $_POST['sup_phn']);
	$sup_addr = mysqli_real_escape_string($conn, $_POST['sup_addr']);
	$t_amnt = mysqli_real_escape_string($conn, $_POST['t_amnt']);
	$p_amnt = mysqli_real_escape_string($conn, $_POST['p_amnt']);
	$d_amnt = mysqli_real_escape_string($conn, $_POST['d_amnt']);
	$dt_amnt = mysqli_real_escape_string($conn, $_POST['dt_amnt']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	if(strlen($dt_amnt)>0){
	  $dt_amnt=explode("-",$dt_amnt);
	  $dt_amnt=mktime(0,0,0,$dt_amnt[1],$dt_amnt[2],$dt_amnt[0]);
	}

	$query ="INSERT INTO `supplier` (`sup_id`, `sup_name`, `sup_address`, `sup_phone`, `amount_total`, `amount_paid`, `amount_due`, `date_paid`,`user_id`) VALUES ('$sup_id','$sup_name','$sup_addr','$sup_phn', '$t_amnt','$p_amnt','$d_amnt','$dt_amnt','$userid')";
	$result = mysqli_query($conn, $query);
	if($result){
		echo $sup_name."~OK";
	}else{
		echo "Error";
		echo $query;

	} 
}
//Ajax request to delete a supplier
if(isset($_GET['oper']) && $_GET['oper'] == "deleteSupplier")
{
	$sp_id = mysqli_real_escape_string($conn, $_POST['supplierid']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="DELETE FROM `supplier` WHERE `sup_id` = '$sp_id' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "OK";
	}else{
		echo "Error";
	} 
}

//Ajax request to supplier updation
if(isset($_POST['oper']) && $_POST['oper'] == "updateSupplier")
{
	$sp_id = mysqli_real_escape_string($conn, $_POST['sup_id']);
	$sup_name = mysqli_real_escape_string($conn, $_POST['sup_name']);
	$sup_phone = mysqli_real_escape_string($conn, $_POST['sup_phone']);
	$sup_address = mysqli_real_escape_string($conn, $_POST['sup_address']);
	$t_amnt = mysqli_real_escape_string($conn, $_POST['t_amnt']);
	$p_amnt = mysqli_real_escape_string($conn, $_POST['p_amnt']);
	$d_amnt = mysqli_real_escape_string($conn, $_POST['d_amnt']);
	$updt_dt = mysqli_real_escape_string($conn, $_POST['dt_amnt']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$b = $updt_dt;
	if(strlen($updt_dt)>0){
	  $updt_dt=explode("/",$updt_dt);
	  $updt_dt=mktime(0,0,0,$updt_dt[0],$updt_dt[1],$updt_dt[2]);
	}

	$query ="UPDATE `supplier` SET `sup_name`='$sup_name',`sup_address`='$sup_address',`sup_phone`='$sup_phone',
	`amount_total`='$t_amnt',`amount_paid`='$p_amnt',`amount_due`='$d_amnt',`date_paid`='$updt_dt' WHERE `sup_id` = '$sp_id' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "OK~".date('d-m-Y','1476558000')."Before convert: ".$b.'After Convert:'.$updt_dt;
	}else{
		echo "Error";
	} 
}

//Ajax request to Show a supplier
if(isset($_GET['oper']) && $_GET['oper'] == "showSupplier")
{	
	$html ="";
	$sp_id = mysqli_real_escape_string($conn, $_POST['sp_id']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="SELECT * FROM `supplier` WHERE `sup_id` = '$sp_id' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		$row = mysqli_fetch_assoc($result);
		$html .= '<dl class="dl-horizontal">
                      <dt>Supplier\'s Name</dt>
                      <dd>'.$row['sup_name'].'</dd>
                      <dt>Supplier\'s Phone no</dt>
                      <dd>'.$row['sup_phone'].'</dd>
                      <dt>Supplier\'s Address</dt>
                      <dd>'.$row['sup_address'].'</dd>
                      <dt>Amount Paid</dt>
                      <dd>'.$row['amount_paid'].'</dd>
                      <dt>Date Paid</dt>';
                      if($row['date_paid'] != ""){
                      	$html .= '<dd>'.date('d-m-Y',$row['date_paid']).'</dd>';
                      }else{
                      	$html .= '<dd>'.$row['date_paid'].'</dd>';
                      }
                      
                      $html .= '<dt>Amount Due</dt>
                      <dd>'.$row['amount_due'].'</dd>
                      <dt>Total Amount</dt>
                      <dd>'.$row['amount_total'].'</dd>
                    </dl>';
        echo $html;
	}
}


////////////////////Maker informaion Updation or deletion Section//////////////////////////
//Ajax request for adding new Maker
if(isset($_POST['oper']) && $_POST['oper'] == "addingMaker")
{
	$maker_id = md5(uniqid(rand()));
	$maker_name = mysqli_real_escape_string($conn, $_POST['maker_name']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="INSERT INTO `maker`(`maker_id`, `maker_name`,`user_id`) VALUES ('$maker_id','$maker_name','$userid')";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "Maker '".$maker_name."' has been added.";
	}else{
		echo "Error";
	} 
}

//Ajax request for a Maker to delete
if(isset($_GET['oper']) && $_GET['oper'] == "deleteMaker")
{
	$mkrid = mysqli_real_escape_string($conn, $_POST['mkrid']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="DELETE FROM `maker` WHERE `maker_id` = '$mkrid' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "OK";
	}else{
		echo "Error";
	} 
}

//Ajax request to maker updation
if(isset($_POST['oper']) && $_POST['oper'] == "updateMaker")
{
	$mkr_id = mysqli_real_escape_string($conn, $_POST['mkr_id']);
	$mkr_name = mysqli_real_escape_string($conn, $_POST['mkr_name']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="UPDATE `maker` SET `maker_name`='$mkr_name' WHERE `maker_id` = '$mkr_id' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "OK";
	}else{
		echo "Error";
	} 
}



////////////////////Type informaion Updation or deletion Section//////////////////////////

//Ajax request for adding new type
if(isset($_POST['oper']) && $_POST['oper'] == "addingType")
{
	$type_id = md5(uniqid(rand()));
	$type_name = mysqli_real_escape_string($conn, $_POST['type_name']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="INSERT INTO `type`(`type_id`, `type_name`,`user_id`) VALUES ('$type_id','$type_name','$userid')";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "Type '".$type_name."' has been added.";
	}else{
		echo "Error";
	} 
}

//Ajax request for a type to delete
if(isset($_GET['oper']) && $_GET['oper'] == "deleteType")
{
	$typid = mysqli_real_escape_string($conn, $_POST['typid']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="DELETE FROM `type` WHERE `type_id` = '$typid' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "OK";
	}else{
		echo "Error";
	} 
}

//Ajax request to maker updation
if(isset($_POST['oper']) && $_POST['oper'] == "updateType")
{
	$typ_id = mysqli_real_escape_string($conn, $_POST['typ_id']);
	$typ_name = mysqli_real_escape_string($conn, $_POST['typ_name']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="UPDATE `type` SET `type_name`='$typ_name' WHERE `type_id` = '$typ_id' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "OK";
	}else{
		echo "Error";
	} 
}


////////////////////Article informaion Updation or deletion Section//////////////////////////
//Ajax request for adding new Article
if(isset($_POST['oper']) && $_POST['oper'] == "addingArticle")
{
	$artcl_id = md5(uniqid(rand()));
	$artcl_dtl = mysqli_real_escape_string($conn, $_POST['artcl_dtl']);
	$size_dtl = mysqli_real_escape_string($conn, $_POST['size_dtl']);
	$clr_dtl = mysqli_real_escape_string($conn, $_POST['clr_dtl']);
	$gender_dtl = mysqli_real_escape_string($conn, $_POST['gender_dtl']);
	$artile_title = mysqli_real_escape_string($conn, $_POST['artile_title']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="INSERT INTO `article`(`article_id`, `article_title`, `article_no`, `gender_name`, `size_no`, `color_name`,`user_id`) VALUES ('$artcl_id','$artile_title','$artcl_dtl','$gender_dtl','$size_dtl','$clr_dtl','$userid')";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "Article Title: '".$artile_title."'  has been added.";
		// echo $query;
	}else{
		echo "Error";
	} 
}
//Ajax request for an article to delete
if(isset($_GET['oper']) && $_GET['oper'] == "deleteArticle")
{
	$artclid = mysqli_real_escape_string($conn, $_POST['artclid']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$query ="DELETE FROM `article` WHERE `article_id` = '$artclid' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "OK";
	}else{
		echo "Error";
	} 
}

//Ajax request to article updation
if(isset($_POST['oper']) && $_POST['oper'] == "updateArticle")
{
	$art_id = mysqli_real_escape_string($conn, $_POST['art_id']);
	$art_no = mysqli_real_escape_string($conn, $_POST['art_no']);
	$art_size = mysqli_real_escape_string($conn, $_POST['art_size']);
	$art_clr = mysqli_real_escape_string($conn, $_POST['art_clr']);
	$art_gndr = mysqli_real_escape_string($conn, $_POST['art_gndr']);
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);
	$art_title = $art_no.'-'.$art_size.'-'.$art_clr.'-'.$art_gndr;

	$query ="UPDATE `article` SET `article_title`='$art_title',`article_no`='$art_no',`gender_name`='$art_gndr',`size_no`='$art_size',`color_name`='$art_clr' WHERE `article_id` = '$art_id' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		echo "OK~".$art_title;
	}else{
		echo "Error";
	} 
}
?>