<?php 
include("../db/dbconfig.php");
include("../db/password_hash.php");
include("../functions/functions.php");

$date = new DateTime();            
$timestamp = $date->getTimestamp();


//Ajax Request for search result.
if(isset($_GET['oper']) && $_GET['oper'] == "getSearchResults")
{	
	$html="";
	$p_id = mysqli_real_escape_string($conn, trim($_POST['q']));
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);

	$pieces = explode("~", $p_id);
	// if(count($pieces) > 1){
		for($i=0; $i<count($pieces); $i++){
				$query ="SELECT p.product_id as p_id, p.maker_id as pr_makerid, maker.maker_name, p.type_id as pr_typeid,
				t.type_name, p.gender_id as pr_genderid, p.article_id as pr_articleid, a.article_no, p.size_id as pr_sizeid,
				p.color_id as pr_colorid, p.purch_price,p.status,p.user_id
				FROM product as p
				left join maker on maker.maker_id = p.maker_id
				left join `type` as t on t.type_id = p.type_id
				left join article as a on a.article_id = p.article_id 
				WHERE p.status = '' AND p.product_id LIKE '%".$pieces[$i]."%' AND a.article_no LIKE '".$pieces[0]."' AND p.user_id = '$userid'
				ORDER BY `status` ASC";
				// echo $query;
				$result = mysqli_query($conn, $query);
				if($result){
					$rowcount=mysqli_num_rows($result);
					if($rowcount > 0){
						while($row = mysqli_fetch_assoc($result)){
							// echo $query."<br>";
							$html .= '<div class="col-md-offset-3 col-md-4">
										<div id="adding_sales" class="box box-primary">
										<div class="box-body">
											<b>Product ID: '.$row['p_id'].'</b><br>
											<b>Shoe Maker:</b>'.$row['maker_name'].'<br>
											<b>Shoe Type:</b>'.$row['type_name'].'<br>
											<b>Wearable:</b>'.$row['pr_genderid'].'<br>
											<b>Article No:</b>'.$row['article_no'].'<br>
											<b>Shoe Size:</b>'.$row['pr_sizeid'].'<br>
											<b>Color:</b>'.$row['pr_colorid'].'<br>'; 
											if($row['status'] == ""){
											$html .= '<button class="btn btn-block btn-success btn-flat disabled">Availible</button><br>
												<input type="text" class="form-control" id="sales_price" placeholder="please enter sales price"><p id="order_error_message"></p>
												<button id="'.$row['p_id'].'" name="add-to-cart-Btn" class="btn btn-block btn-info btn-xs">Add To Cart</button>';
											}else{
												$html .= '<button class="btn btn-block btn-danger btn-flat disabled">Sold</button>';
											}
										$html .='<br>
										</div>
										</div>
									</div>';
						}
						echo $html;
						$html ="";
					}else{
						echo "Error";
					}
				}else{
					// echo $query;
					echo "Error";
				}
				// $pieces ="";					
		}
}

//Ajax Request for search result, which are sold
if(isset($_GET['oper']) && $_GET['oper'] == "getReplaceSearchResults")
{	
	$html="";
	$p_id = mysqli_real_escape_string($conn, trim($_POST['q']));
	$userid = mysqli_real_escape_string($conn, $_POST['userid']);

	$pieces = explode("~", $p_id);
	// if(count($pieces) > 1){
		for($i=0; $i<count($pieces); $i++){
				$query ="SELECT p.product_id as p_id, p.maker_id as pr_makerid, maker.maker_name, p.type_id as pr_typeid,
				t.type_name, p.gender_id as pr_genderid, p.article_id as pr_articleid, a.article_no, p.size_id as pr_sizeid,
				p.color_id as pr_colorid, p.purch_price,p.status,so.sales_price,so.sales_time,p.user_id
				FROM product as p
				left join maker on maker.maker_id = p.maker_id
				left join `type` as t on t.type_id = p.type_id
				left join article as a on a.article_id = p.article_id
				left join sales_order_detail sd on sd.product_id=p.product_id
				left join sales_order so on so.so_id=sd.so_id
				WHERE p.product_id LIKE '%".$pieces[$i]."%' AND `status` = 'SOLD' AND a.article_no LIKE '".$pieces[0]."' AND p.user_id = '$userid'
				ORDER BY `status` ASC";
				// echo $query;
				$result = mysqli_query($conn, $query);
				if($result){
					$rowcount=mysqli_num_rows($result);
					if($rowcount > 0){
						while($row = mysqli_fetch_assoc($result)){
							// echo $query."<br>";
							$html .= '<div class="col-md-offset-3 col-md-4">
										<div id="adding_sales" class="box box-primary">
										<div class="box-body">
											<b>Product ID: '.$row['p_id'].'</b><br>
											<b>Shoe Maker:</b>'.$row['maker_name'].'<br>
											<b>Shoe Type:</b>'.$row['type_name'].'<br>
											<b>Wearable:</b>'.$row['pr_genderid'].'<br>
											<b>Article No:</b>'.$row['article_no'].'<br>
											<b>Shoe Size:</b>'.$row['pr_sizeid'].'<br>
											<b>Color:</b>'.$row['pr_colorid'].'<br>
											<b>SOLD FOR: '.$row['sales_price'].'/Rs</b><br>
											<b>SOLD Time: '.date('h:m:sa',$row['sales_time']).'</b><br> 
											<b>SOLD Date: '.date('m/d/Y',$row['sales_time']).'</b><br>';				
							$html .= '<button id="'.$row['p_id'].'" class="btn btn-block btn-warning btn-flat" name="replace-Btn" >Return this shoes</button>';
										$html .='<br>
										</div>
										</div>
									</div>';
						}
						echo $html;
						$html ="";
					}else{
						echo "Error";
					}
				}else{
					echo "Error";
				}					
		}	
}

//Ajax Request To add Purchase
if(isset($_POST['action']) &&  $_POST['action']==="addExpense")
{
	$expense_id =  md5(uniqid(rand()));
	$user_id=$_POST['userid'];
	$amnt = $_POST['amnt'];
	$desc = $_POST['desc']; 
   
   	$query= "INSERT INTO `extra_expense`(`expense_id`, `expense_amnt`, `expense_dtl`, `expense_date`,`user_id`) VALUES ('$expense_id','$amnt','$desc','$timestamp','$user_id');";
    $result = mysqli_query($conn,$query);
    if($result){
    	echo "OK";           
    }else{
    	echo "ERROR";
    }
}

//Upatinfo sold status ajax request
if(isset($_GET['oper']) && $_GET['oper'] == "solditemsStatus")
{	
	$user_id=$_POST['userid'];
	$productids=$_POST['productids'];
	$productPrices=$_POST['productPrices'];
	$idPieces = explode("~", $productids);
	$pricePieces = explode("~", $productPrices);
	for($i = 0; $i< count($idPieces) -1 ; $i++){
		$so_id=  md5(uniqid(rand()));
		$sod_id=  md5(uniqid(rand()));
		$query ="UPDATE `product` SET `status`= 'SOLD' WHERE `product_id`= '".$idPieces[$i]."';";
		$sales_order_query = "INSERT INTO `sales_order`(`so_id`, `sales_time`, `sales_price`, `user_id`) VALUES ('$so_id',
		'$timestamp','".$pricePieces[$i]."','$user_id');";
		$sales_order_detail_query ="INSERT INTO `sales_order_detail`(`sod_id`, `so_id`, `product_id`, `user_id`) VALUES ('$sod_id','$so_id','".$idPieces[$i]."','$user_id');";
		mysqli_query($conn, $query);
		mysqli_query($conn, $sales_order_query);
		mysqli_query($conn, $sales_order_detail_query);
	} 
}

//Upating return/replace status ajax request
if(isset($_GET['oper']) && $_GET['oper'] == "returnSales")
{	
	$p_id=$_POST['p_id'];
	$getting_ids = "SELECT sod_id,so_id FROM sales_order_detail WHERE product_id ='$p_id'";
	$result = mysqli_query($conn, $getting_ids);
	if($result){
		$row=mysqli_fetch_assoc($result);

		$dlt_sod ="DELETE FROM `sales_order_detail` WHERE `sod_id`= '".$row['sod_id']."';";
		$dlt_so = "DELETE FROM `sales_order` WHERE `so_id` = '".$row['so_id']."';";
		$updt_prod ="UPDATE `product` SET `status`= '' WHERE `product_id`= '$p_id';";

		// echo $dlt_sod."<br>".$dlt_so."<br>".$updt_prod;
		mysqli_query($conn, $dlt_sod);
		mysqli_query($conn, $dlt_so);
		mysqli_query($conn, $updt_prod);

		echo "OK";
	}else{
		echo "ERROR";
	}	
}

//Ajax request for getting Makers
// if(isset($_GET['oper']) && $_GET['oper'] == "getMakers")
// {
// 	$r =[];
// 	$query ="SELECT * FROM `maker`;";
// 	$result = mysqli_query($conn, $query);
// 	if($result){
// 		while($row = mysqli_fetch_assoc($result)){
// 			$r[] = $row;
// 		}
// 		echo json_encode($r);
// 	}else{
// 		echo "Error";
// 	} 
// }

// //Ajax request for getting Types
// if(isset($_GET['oper']) && $_GET['oper'] == "getTypes")
// {
// 	$r =[];
// 	$query ="SELECT * FROM `type`;";
// 	$result = mysqli_query($conn, $query);
// 	if($result){
// 		while($row = mysqli_fetch_assoc($result)){
// 			$r[] = $row;
// 		}
// 		echo json_encode($r);
// 	}else{
// 		echo "Error";
// 	} 
// }

// //Ajax request for getting Gender
// if(isset($_GET['oper']) && $_GET['oper'] == "getGender")
// {
// 	$r =[];
// 	$query ="SELECT * FROM `gender`;";
// 	$result = mysqli_query($conn, $query);
// 	if($result){
// 		while($row = mysqli_fetch_assoc($result)){
// 			$r[] = $row;
// 		}
// 		echo json_encode($r);
// 	}else{
// 		echo "Error";
// 	} 
// }

// //Ajax request for getting Article
// if(isset($_GET['oper']) && $_GET['oper'] == "getArticles")
// {
// 	$r =[];
// 	$query ="SELECT * FROM `article`;";
// 	// $query ="SELECT * FROM `product` WHERE maker_id`='$makerid' && `type_id`='$typeid' && `gender_id`='$genderid';";	
// 	$result = mysqli_query($conn, $query);
// 	if($result){
// 		while($row = mysqli_fetch_assoc($result)){
// 			$r[] = $row;
// 		}
// 		echo json_encode($r);
// 	}else{
// 		echo "Error";
// 	} 
// }


// }else{
	// 	$query ="select p.product_id as p_id, p.maker_id as pr_makerid, maker.maker_name, p.type_id as pr_typeid,
	// 			t.type_name, p.gender_id as pr_genderid, g.gender_name, p.article_id as pr_articleid, a.article_no,
	// 			p.size_id as pr_sizeid, s.size_no, p.color_id as pr_colorid, c.color_name, p.purch_price,p.status
	// 			from product as p
	// 			left join maker on maker.maker_id = p.maker_id
	// 			left join `type` as t on t.type_id = p.type_id
	// 			left join gender as g on g.gender_id = p.gender_id
	// 			left join article as a on a.article_id = p.article_id
	// 			left join size as s on s.size_id = p.size_id
	// 			left join color as c on c.color_id = p.color_id 
	// 			WHERE `product_id` LIKE '%".$p_id."%'
	// 			group by g.gender_name;";
	// 	$result = mysqli_query($conn, $query);
	// 	if($result){
	// 		while($row = mysqli_fetch_assoc($result)){
	// 			$html .= '<div class="col-md-offset-3 col-md-4">
	// 						<div id="adding_sales" class="box box-primary">
	// 						<div class="box-body">
	// 				              <b>Product ID: '.$row['p_id'].'</b><br>
	// 				              <b>Shoe Maker:</b>'.$row['maker_name'].'<br>
	// 				              <b>Shoe Type:</b>'.$row['type_name'].'<br>
	// 				              <b>Wearable:</b>'.$row['gender_name'].'<br>
	// 				              <b>Article No:</b>'.$row['article_no'].'<br>
	// 				              <b>Shoe Size:</b>'.$row['size_no'].'<br>
	// 				              <b>Color:</b>'.$row['color_name'].'<br>
	// 				              <b>Status:</b>'; 
	// 				              if($row['status'] == ""){
	// 				              $html .= '<button class="btn btn-block btn-success btn-flat disabled">Availible</button>';
	// 				          	  }else{
	// 				          	  	$html .= '<button class="btn btn-block btn-danger btn-flat disabled">Sold</button>';
	// 				          	  }
	// 				        $html .='<br>
	// 						</div>
	// 						</div>
	// 					</div>';
	// 		}
	// 		echo $html;
	// 	}else{
	// 		// echo $query;
	// 		echo "Error";
	// 	} 
	// }

?>