<?php 
include("../db/dbconfig.php");
include("../db/password_hash.php");
include("../functions/functions.php");

$date = new DateTime();            
$timestamp = $date->getTimestamp();

//Ajax Request To add Purchase
if(isset($_POST['action']) &&  $_POST['action']==="addPurchase")
{
	$user_id=$_POST['userid'];
	$supplier_id=$_POST['supplier_id'];
	date_default_timezone_set("Asia/Karachi");           
	$time= date("h:i:sa");
	            
	$po_id=  md5(uniqid(rand()));
   
   	$query= "INSERT INTO `purchase_order`(`po_id`, `date`, `time`, `sup_id`, `user_id`) VALUES ('$po_id','$timestamp','$time','$supplier_id','$user_id');";
     mysqli_query($conn,$query);
     echo $po_id;           
}

//Ajax Request to add Product
if(isset($_POST['action']) &&  $_POST['action']==="addProduct")
{
	$artcl_name = "";
	$gndr_name = "";
	$clr_name = "";
	$po_id=$_POST['po_id'];
	$user_id=$_POST['userid'];
	$makerid=$_POST['makerid'];
	$typeid=$_POST['typeid'];
	$article_id=$_POST['articleid'];
	$article_number=$_POST['article_number'];
	$size_val=$_POST['size_val'];
	$clr_val=$_POST['clr_val'];
	$gender_val=$_POST['gender_val'];
	$another_name=$_POST['another_name'];
	$pr_price=$_POST['pr_price'];
	$shoes_quantity=$_POST['shoes_quantity'];
	$shoes_quantity = intval($shoes_quantity);
	            
	$p_id = $article_number."-".$gender_val."-".$clr_val;
	$pod_id=  md5(uniqid(rand()));
	for($i=1; $i <= $shoes_quantity; $i++){
		$timestamp = $date->getTimestamp();
		$p_id = $article_number."-".$gender_val."-".$clr_val."-".$timestamp."".$i;
		$pod_id=  md5(uniqid(rand()));
		$query ="INSERT INTO `product`(`product_id`, `purch_price`, `purch_time`, `color_id`, `size_id`, `article_id`, `maker_id`, `type_id`, `gender_id`, `title`,`user_id`) VALUES('$p_id','$pr_price','$timestamp', '$clr_val','$size_val','$article_id','$makerid','$typeid','$gender_val','$another_name','$user_id');";   
		$query_detail= "INSERT INTO `purchase_order_detail`(`pod_id`, `product_id`, `user_id`,`po_id`) VALUES ('$pod_id','$p_id','$user_id','$po_id');";
		$result = mysqli_query($conn, $query);
		// echo $query;
		if(!$result){
	    	echo "ERROR";
	    	// echo $query;
	    	break;
		    	
	    }else{
		    mysqli_query($conn,$query_detail);
		    echo "OK";
		    // echo "Last Query........................................................................";
		    // echo $query;
		    // echo $query_detail;
	    }
	}	          
}

//Ajax request for getting Supplier
if(isset($_GET['oper']) && $_GET['oper'] == "getSuppliers")
{
	$r =[];
	$userid = mysqli_real_escape_string($conn, $_GET['userid']);
	$query ="SELECT * FROM `supplier` WHERE `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		while($row = mysqli_fetch_assoc($result)){
			$r[] = $row;
		}
		echo json_encode($r);
	}else{
		echo "Error";
	} 
}

//Ajax request for getting Makers
if(isset($_GET['oper']) && $_GET['oper'] == "getMakers")
{
	$r =[];
	$userid = mysqli_real_escape_string($conn, $_GET['userid']);
	$query ="SELECT * FROM `maker` WHERE `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		while($row = mysqli_fetch_assoc($result)){
			$r[] = $row;
		}
		echo json_encode($r);
	}else{
		echo "Error";
	} 
}

//Ajax request for getting Types
if(isset($_GET['oper']) && $_GET['oper'] == "getTypes")
{
	$r =[];
	$userid = mysqli_real_escape_string($conn, $_GET['userid']);
	$query ="SELECT * FROM `type` WHERE `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		while($row = mysqli_fetch_assoc($result)){
			$r[] = $row;
		}
		echo json_encode($r);
	}else{
		echo "Error";
	} 
}

//Ajax request for getting Article
if(isset($_GET['oper']) && $_GET['oper'] == "getArticles")
{
	$r =[];
	$userid = mysqli_real_escape_string($conn, $_GET['userid']);
	$query ="SELECT * FROM `article` WHERE `user_id` = '$userid';";
	// echo $query;
	$result = mysqli_query($conn, $query);
	if($result){
		while($row = mysqli_fetch_assoc($result)){
			$r[] = $row;
		}
		echo json_encode($r);
	}else{
		echo "Error";
	} 
}

//Ajax request for fetching article details.
if(isset($_GET['oper']) && $_GET['oper'] == "articleDetail")
{
	$r =[];
	$article_id=$_GET['article_id'];
	$userid = mysqli_real_escape_string($conn, $_GET['userid']);
	$query ="SELECT * FROM `article` WHERE `article_id` = '$article_id' AND `user_id` = '$userid';";
	$result = mysqli_query($conn, $query);
	if($result){
		while($row = mysqli_fetch_assoc($result)){
			$r[] = $row;
		}
		echo json_encode($r);
	}else{
		echo "Error";
	} 
}


//Ajax request for getting purchase order detail w.r.t supplier
if(isset($_GET['oper']) && $_GET['oper'] == "purchaseOrderDetail")
{	
	$html ="";
	$i =0;
	$suppl_id = mysqli_real_escape_string($conn,$_GET['suppl_id']);
	$userid = mysqli_real_escape_string($conn, $_GET['userid']);
	$query ="SELECT p.product_id, p.purch_price,p.purch_time,ar.article_title, ar.article_no, ar.color_name, ar.size_no,
			ar.gender_name, p.title, p.status ,pod.pod_id as purc_order_dtl_id, p.user_id
			from supplier AS s
			LEFT JOIN purchase_order AS po ON po.sup_id = s.sup_id
			LEFT JOIN purchase_order_detail AS pod ON pod.po_id = po.po_id
			LEFT JOIN product AS p ON p.product_id = pod.product_id
			LEFT JOIN article AS ar ON ar.article_id = p.article_id
			WHERE s.sup_id = '$suppl_id' AND po.sup_id != '' AND  p.user_id = '$userid'
			ORDER BY p.product_id;";
			// echo $query;
	$result = mysqli_query($conn, $query);
	if($result){
		while($row = mysqli_fetch_assoc($result)){
			$i++;
			$html .= "<tr id='".$row['product_id']."' pod-id='".$row['purc_order_dtl_id']."'>";
				if($row['status'] != "SOLD"){                              
					$html .= "<td><button name='dlt-btn-sup-prdct' class='btn btn-danger'><i class='fa fa-trash'></i></button></td>";
				}else{
					$html .= "<td><button name='dlt-btn-sup-prdct' class='btn btn-danger' disabled><i class='fa fa-trash'></i></button></td>";
				}

		    $html .= "<td>{$row['article_title']}</td>
		                <td>{$row['purch_price']}</td>
	                  	<td>".date('m/d/Y h:m:sa', $row['purch_time'])."</td>
	                  	<td>{$row['title']}</td>
	                  	<td>{$row['status']}</td>
	                </tr>    ";
		}
		echo $html;
	}else{
		echo "Error";
	} 
}

//Delete request for product and purcahse order detail of the product
if(isset($_GET['oper']) && $_GET['oper'] == "deleteProductandPurchaseOrder")
{
	$p_id = mysqli_real_escape_string($conn, $_POST['p_id']);
	$pod_id = mysqli_real_escape_string($conn, $_POST['pod_id']);
	$query ="DELETE FROM `product` WHERE `product_id` = '$p_id';";
	$query2 ="DELETE FROM `purchase_order_detail` WHERE `pod_id` = '$pod_id' AND `product_id` = '$p_id';";
	$result = mysqli_query($conn, $query);
	if($result){
		$result = mysqli_query($conn, $query2);
		if($result){
			echo "OK";
		}else{
			echo "error";
		}
	}else{
		echo "Error";
	} 
}


//Ajax request for getting Size
// if(isset($_GET['oper']) && $_GET['oper'] == "getSizes")
// {
// 	$r =[];
// 	$query ="SELECT * FROM `size`;";
// 	$result = mysqli_query($conn, $query);
// 	if($result){
// 		while($row = mysqli_fetch_assoc($result)){
// 			$r[] = $row;
// 		}
// 		echo json_encode($r);
// 	}else{
// 		echo "Error";
// 	} 
// }

//Ajax request for getting Color
// if(isset($_GET['oper']) && $_GET['oper'] == "getColors")
// {
// 	$r =[];
// 	$query ="SELECT * FROM `color`;";
// 	$result = mysqli_query($conn, $query);
// 	if($result){
// 		while($row = mysqli_fetch_assoc($result)){
// 			$r[] = $row;
// 		}
// 		echo json_encode($r);
// 	}else{
// 		echo "Error";
// 	} 
// }

//Ajax request for getting Gender
// if(isset($_GET['oper']) && $_GET['oper'] == "getGender")
// {
// 	$r =[];
// 	$query ="SELECT * FROM `gender`;";
// 	$result = mysqli_query($conn, $query);
// 	if($result){
// 		while($row = mysqli_fetch_assoc($result)){
// 			$r[] = $row;
// 		}
// 		echo json_encode($r);
// 	}else{
// 		echo "Error";
// 	} 
// }


?>