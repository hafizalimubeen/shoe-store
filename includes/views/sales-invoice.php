<?php 
include '../db/dbconfig.php';
$product_id=$_REQUEST['solditems'];
$product_price=$_REQUEST['soldprice'];
// echo $product_id;
$total =0;
$idpieces = explode("~", $product_id);
$date = new DateTime();
$timestamp = $date->getTimestamp();
?>
<html><head></head><body><link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="../plugins/iCheck/flat/blue.css">
<link rel="stylesheet" href="../plugins/morris/morris.css">
<link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">
<div class="col-md-6" style="/*max-width: 20%;*/margin: 2px;padding: 8px;border-top: 1px solid;border-bottom: 1px solid;text-align: center;">
<small>Sale Date: <?php echo date('m/d/Y', $timestamp) ?></small><br>
<small>Sale Time: <?php echo date('h:m:sa', $timestamp) ?></small>
<h1><span style="border-bottom:2px solid;">Servis Shoes</span></h1>
 <?php 
 for($i=0; $i<count($idpieces) -1; $i++){
  $q="select p.product_id as p_id, p.maker_id as pr_makerid, maker.maker_name, p.type_id as pr_typeid,
          t.type_name, p.gender_id as pr_genderid, p.article_id as pr_articleid, a.article_no,
          p.size_id as pr_sizeid, p.color_id as pr_colorid, p.purch_price,p.status
          from product as p
          left join maker on maker.maker_id = p.maker_id
          left join `type` as t on t.type_id = p.type_id
          left join article as a on a.article_id = p.article_id
          WHERE `product_id` LIKE '%".$idpieces[$i]."%'";
          $result=mysqli_query($conn,$q);
          $row=mysqli_fetch_assoc($result);

          $q2 = "select sales_price, count(sod_id) as numberofsales 
              from sales_order_detail as sod
              left join sales_order as so on so.so_id = sod.so_id
              where sod.product_id = '".$idpieces[$i]."'
              GROUP BY sales_price;";
              $result2 = mysqli_query($conn, $q2);
              $row2=mysqli_fetch_assoc($result2);
              $total += $row2['sales_price'];
?>
  <div class="col-md-offset-3 col-md-4">
    <div id="adding_sales" class="box box-primary">
      <div class="box-body">
                <span><b>Product <?php echo ($i+1); ?> : <?php $pieces = explode('-', $row['p_id']); echo $pieces[3];  ?></b><br>
                <b>Company:</b><?php echo $row['maker_name']; ?><br>
                <b>Shoe Type:</b><?php echo $row['type_name']; ?><br>
                <b>Wearable:</b><?php echo $row['pr_genderid']; ?><br>
                <b>Article No:</b><?php echo $row['article_no']; ?><br>
                <b>Shoe Size:</b><?php echo $row['pr_sizeid']; ?><br>
                <b>Color:</b><?php echo $row['pr_colorid']; ?><br> 
                <b>Sale Price:</b><?php echo $row2['sales_price']; ?><br><br> </span>
                
      </div>
    </div>
  </div>     
<?php 
}
 ?>
  <span style="border-bottom:2px solid;border-top:2px solid;"><b>Net Total:</b><?php echo $total; ?><br><br> </span>
  <h4>Thank You for shopping here.</h4>

 <?php

?>

</div>
</body>
</html>