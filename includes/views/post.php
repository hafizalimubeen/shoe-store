
              <!-- Box Comment -->
              <div class="box  box-success">
                <div class="box-header with-border">
                  <div class="user-block">
                    <img class="img-circle " src="././dist/img/user.png" alt="user image">
                    <span id="user" class="username"><a href="#">Jonathan Burke Jr.</a></span>
                    <span id="desc" class="description">7:30 PM Today</span>
                  </div><!-- /.user-block -->
                  <div class="box-tools">
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div id="detail" class="box-body" style="display: block;">
                  <!-- post text -->
                  <p>Far far away, behind the word mountains, far from the
                    countries Vokalia and Consonantia, there live the blind
                    texts. Separated they live in Bookmarksgrove right at
                  the coast of the Semantics, a large language ocean.
                    A small river named Duden flows by their place and supplies
                    it with the necessary regelialia. It is a paradisematic
                    country, in which roasted parts of sentences fly into
                    your mouth.</p>

                  <!-- Attachment -->
                  <div class="attachment-block clearfix">
                    <a><i class="fa fa-paperclip "> &nbsp;</i>File Attached</a>
                  </div><!-- /.attachment-block -->

                  <!-- Social sharing buttons -->
                  <span id="comment_count" class="pull-right text-muted"> 2 comments</span>
                </div><!-- /.box-body -->
                <div class="box-footer box-comments" style="display: block;">
                  <div class="box-comment">
                    <!-- User image -->
                    <img class="img-circle img-sm" src="././dist/img/user.png" alt="user image">
                    <div class="comment-text">
                      <span class="username">
                        Maria Gonzales
                        <span class="text-muted pull-right">8:03 PM Today</span>
                      </span><!-- /.username -->
                      It is a long established fact that a reader will be distracted
                      by the readable content of a page when looking at its layout.
                    </div><!-- /.comment-text -->
                  </div><!-- /.box-comment -->
                </div><!-- /.box-footer -->
                <div class="box-footer" style="display: block;">
                  <form action="#" method="post">
                    <img class="img-responsive img-circle img-sm" src="././dist/img/user.png" alt="alt text">
                    <!-- .img-push is used to add margin to elements next to floating images -->
                    <div class="img-push">
                        <input type="text" class="form-control input-sm" placeholder="Press enter to post comment" ><button name="btn-user-active" class="btn bg-green">Activate</button>
                        <a id="file-attach"><i class="fa fa-paperclip" style="float: right;margin-right: 6px;margin-top: -20px;position: relative;z-index: 2;"></i> </a>
                      <p id ="attachment" class="attachment"><a><i class="fa fa-paperclip "> &nbsp;</i>File Attached 1</a><a class="remove"><i class="fa fa-remove "> </i>Remove</a></p>
                      <p class="attachment"><a><i class="fa fa-paperclip "> &nbsp;</i>File Attached 2</a><a class="remove"><i class="fa fa-remove "> </i>Remove</a></p>                    
                    </div>
                    
                  </form>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
           