<?php include('includes/views/session.php');?>
<!DOCTYPE html>
<html>
  <!--head-->
  <?php include('includes/html-parts/head.php');?>
  <!--head-->
  <body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
    <!-- header -->
      <?php include('includes/views/header.php');?>
      <!-- Left side column. contains the logo and sidebar -->
                 <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>        
              <?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?>
              </p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span> 
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->

          </ul><!-- /.sidebar-menu --> 
          <ul class="sidebar-menu">
            <li class=""><a href="sales.php"><i class="fa fa-folder"></i>Create Sales<i class="fa fa-angle-left pull-right"></i></a> </li>
            <li class=""><a href="expense.php"><i class="fa fa-folder"></i>Extra Expense<i class="fa fa-angle-left pull-right"></i></a></li>
            <li class="active"><a href="return.php"><i class="fa fa-folder"></i>Return Sales<i class="fa fa-angle-left pull-right"></i></a></li>
          </ul><!-- /.sidebar-menu --> 
        </section> 
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>            
              <i class="ion ion-bag" style="margin-left: 50px;color:#57bddb;"></i>Return/Replace a Sale
          </h1>                     
        </section> 


      <div class=" col-md-12" style="background: white;padding-top: 30px;margin-top:25px;">
        <div class="col-md-offset-2 col-md-6">
            <div id="search-sales-box" class="input-group margin">
              <input type="text" class="form-control" id="serachq-rp" placeholder="enter shoes Article Number">
              <span class="input-group-btn">
                <button id="search-id-rp-btn" class="btn btn-info btn-flat" type="button">Search</button>
              </span>
            </div>
          <small>Note: Only search with article number. . .</small>
          <p id="sales-error-msg" style="color: red; font-weight: bold;"></p>
        </div>
        <div id="search-results">
          
        </div>
      </div>                                                         




      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include('includes/views/footer.php');?>

      <!-- Control Sidebar -->
      <?php include('includes/views/rightbar.php');?>
      
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
        <!-- <script>
             var map;
             function initMap(){                
                    map = new google.maps.Map(document.getElementById('world-map'), {
                    center: {lat: 30.774505540304002, lng: 72.43481934070587},
                    zoom: 16                   
                    });        
                   var marker=new google.maps.Marker({
                        position:{lat: 30.774505540304002,lng: 72.43481934070587},
                        map:map,
                   });              
             }        
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsogWjl6FoMi73yLHQJcPe5E7yDf7xBng&callback=initMap"
    async defer></script> -->
  </body>
</html>