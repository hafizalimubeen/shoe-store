<?php include('includes/views/session.php');?>
<!DOCTYPE html>
<html>
  <!--head-->
  <?php include('includes/html-parts/head.php');?>
  <!--head -->
  <body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
    <!-- header -->
      <?php include('includes/views/header.php');?>
      <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>        
              <?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?>
              </p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span> 
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">Admin Panel Menu </li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="admin.php">
                <i class="fa fa-folder"></i> <span>Admin Panel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a></li>
            <li><a href="update_admin.php">
                <i class="fa fa-circle"></i>
                <span>Update Admin Panel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a></li>
          </ul><!-- /.sidebar-menu --> 
        </section> 
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
              <a id="sup" href="#"><i class="fa fa-briefcase" style="margin-left: 40px;color:#57bddb;"></i>Add New Supplier</a>
              <a id="maker" href="#"><i class="fa fa-gears" style="margin-left: 40px;color:#57bddb;"></i>Add New Maker</a> 
              <a id="typ" href="#"><i class="fa fa-industry" style="margin-left: 40px;color:#57bddb;"></i>Add New Type</a>
              <a id="artcl" href="#"><i class="fa fa-hand-lizard-o" style="margin-left: 40px;color:#57bddb;"></i>Add New Article</a>
<!--               <a id="clr" href="#"><i class="fa fa-circle" style="margin-left: 40px;color:#57bddb;"></i>New Color</a>
              <a id="siz" href="#"><i class="fa fa-hand-grab-o" style="margin-left: 40px;color:#57bddb;"></i>Add New Size</a> -->       
          </h4>                     
        </section> 

        <div class=" col-md-12" style="background: white;padding-top: 30px;margin-top:25px;">
        <div class="col-md-offset-3 col-md-6">           
              <!--  -->
              <!---Add new Article -->
              <!-- -->
              <div id="article_detail" class="box box-primary">
                  
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Make an Article</b></h3>
                </div>
                <div id="model_detail">
                 
                    <div class="box-body">
                        <div class="form-group">
                            <label>Article Name:</label>
                            <input type="text" class="form-control" id="article-dtl" placeholder="Enter article"/>
                        </div>
                        <div class="form-group">
                            <label>Select Shoe Size:</label>
                            <select class="form-control" id="size_dtl">
                                <option value="" disabled selected>Please Select a Size</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19"19></option>
                            </select> 
                        </div>
                        <div class="form-group">
                            <label>Color Name:</label>
                            <input type="text" class="form-control" id="color-dtl" placeholder="Enter color"/>
                        </div>
                        <div class="form-group">
                            <label>Select Gender:</label>
                            <select class="form-control" id="gender_dtl">
                                <option value="" disabled selected>Please Select a Gender</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select> 
                        </div>
                        <div class="form-group">
                            <h3 id="article-title-display"></h3>
                        </div>

                    </div>                    
                  
                  <div id="article_error_message" class="validatr-message" style="color: rgb(240, 68, 77); border: 1px solid rgb(228, 166, 175); padding: 2px 6px; border-radius: 0px; position: relative; left: 19px; top: 0px; background-color: rgb(255, 203, 203);">
                   </div>                                                          
                  <div class="box-footer">
                    <button id="submit_article" class="btn btn-primary">Submit</button>
                  </div>
                    
                </div>
              </div>
              <!--- -->
              <!---article Details End -->
              <!-- -->
              
              <!--- -->
              <!---Maker Started -->
              <!-- --> 
              <div id="maker_add" class="box box-primary">                  
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Add New Maker</b></h3>
                </div>
                  
                <div id="maker_details">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="username">Please Add The Name Of Some Manufacturing Company Here That You Deal With                          
                      For Example;
                        <ul>
                            <li>Bata</li>
                            <li>Service</li>
                            <li>Borjan</li>
                            <li>Stylo</li>
                            <li>Metro</li>
                        </ul>
                      </label>
                    </div>
                      
                    <div class="form-group">
                      <label for="maker_name">Maker Name:</label>
                      <input type="text" class="form-control" id="maker_name" placeholder="Enter Maker Name Here">
                    </div>                                        
                  </div>
                    <div id="merror_message" class="validatr-message" style="color: rgb(240, 68, 77); border: 1px solid rgb(228, 166, 175); padding: 2px 6px; border-radius: 0px; position: relative; left: 19px; top: 0px; background-color: rgb(255, 203, 203);">
                   </div>                                                                                              
                    
                  <div class="box-footer">
                    <button id="submit_maker" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </div>
              <!-- -->
              <!---Maker Ended -->
              <!-- -->


              <!-- -->
              <!--Type Started -->
              <!-- --> 
              <div id="type_add" class="box box-primary">                  
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Add New Type of shoes</b></h3>
                </div>
                  
                <div id="type_details">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="username">Please Add Some Types of 'Shoewear' Here That You Deal With                          
                      For Example;
                        <ul>
                            <li>Sandal</li>
                            <li>Sneakers</li>
                            <li>Slippers</li>
                            <li>Joggers</li>
                            <li>High Heels</li>
                        </ul>
                      </label>
                    </div>
                      
                    <div class="form-group">
                      <label for="type_name">Type Name:</label>
                      <input type="text" class="form-control" id="type_name" placeholder="Enter Type Name Here">
                    </div>                                        
                  </div>
                    <div id="type_error_message" class="validatr-message" style="color: rgb(240, 68, 77); border: 1px solid rgb(228, 166, 175); padding: 2px 6px; border-radius: 0px; position: relative; left: 19px; top: 0px; background-color: rgb(255, 203, 203);">
                   </div>                                                                                              
                    
                  <div class="box-footer">
                    <button id="submit_type" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </div>
              <!-- -->
              <!---Type Ended -->
              <!-- -->
              

              <!--- -->
              <!---Supplier Details -->
              <!-- -->
              <div id="sup_details" class="box box-primary">                  
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Supplier Details</b></h3>
                </div>
                <div id="sup_detail" class="form-horizontal">
                     <div class="box-body"> 
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="s_name">Supplier Name</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="s_name" placeholder="Enter Supplier Name">
                          </div>
                        </div>                
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="s_name">Supplier Phone</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="s_phone" placeholder="Enter Supplier Phone">
                          </div>
                        </div>                                        
                      
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="s_name">Supplier Address</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="s_address" placeholder="Enter Supplier Address">
                          </div>
                        </div>
                        <div class="form-group">
                          <label>
                            <input id="addtnl-supplier" type="checkbox" class="flat-red" >
                            Additional Information
                          </label>
                          <p class="col-offset-2 small">add additional information of billing(If any)</p>
                        </div>              
                        <div id="addtnl-supplier-body"> 
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="t_amnt">Total Amount</label>
                              <div class="col-sm-8">
                                <input type="text" class="form-control" id="t_amnt" placeholder="Enter total amount to be paid.">
                              </div>
                              
                            </div>                
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="p_amnt">Amount Paid</label>
                              <div class="col-sm-8">
                                <input type="text" class="form-control" id="p_amnt" placeholder="Amount paid to the supplier.">
                              </div>
                            </div>                                        
                            <div class="form-group" id="d_amnt-grp">
                              <label class="col-sm-4 control-label" for="d_amnt">Amount Due</label>
                              <div class="col-sm-8">
                              <input type="text" class="form-control" id="d_amnt" readonly="" placeholder="Remaining Balance To Be Paid">
                              </div>
                            </div> 
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="dt_amnt">Date</label>
                              <div class="col-sm-8">
                              <input type="date" class="form-control" id="dt_amnt" placeholder="Enter Date of amount paying">
                              </div>
                            </div>                                        
                        </div> 
                    </div>                                                                             
                  </div>
                  <div id="serror_message" class="validatr-message" style="color: rgb(240, 68, 77); border: 1px solid rgb(228, 166, 175); padding: 2px 6px; border-radius: 0px; position: relative; left: 19px; top: 0px; background-color: rgb(255, 203, 203);">
                   </div>                                                          
                  <div class="box-footer">
                    <button id="submit_sup" class="btn btn-primary btn-block">Submit</button>
                  </div>
                </div>
              </div>
              <!-- -->
              <!---Supplier Details End -->
              <!-- -->
                    
</div>                                                       

      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include('includes/views/footer.php');?>

      <!-- Control Sidebar -->
      <?php include('includes/views/rightbar.php');?>
      
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
        <!-- <script>
             var map;
             function initMap(){                
                    map = new google.maps.Map(document.getElementById('world-map'), {
                    center: {lat: 30.774505540304002, lng: 72.43481934070587},
                    zoom: 16                   
                    });        
                   var marker=new google.maps.Marker({
                        position:{lat: 30.774505540304002,lng: 72.43481934070587},
                        map:map,
                   });              
             }        
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsogWjl6FoMi73yLHQJcPe5E7yDf7xBng&callback=initMap"
    async defer></script> -->
  </body>
</html>