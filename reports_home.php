<?php include('includes/views/session.php');?>
<?php 

// include("./includes/db/dbconfig.php");
//   if(session_status() != PHP_SESSION_ACTIVE)
//   {
//       ini_set('session.gc_maxlifetime', 86400);
//       session_start();  
//   }
//   if(!isset($_SESSION['userSession']))
//   {
//       header("Location: index.php");
//   }

//   $userid=$_SESSION['userSession'];
//   $username= $_SESSION['username'];
//   $fullname = $_SESSION['fullname'];
//   $user_type=$_SESSION['usertype'];

//   if(isset($_SESSION['userSession']))
//     echo "<script> userid = '".$_SESSION['userSession']."';</script>";

//     if(isset($_SESSION['userSession']))
//     echo "<script> username = '".$_SESSION['username']."';</script>";

//   if(isset($_SESSION['usertype']))
//     echo "<script> usertype = '".$_SESSION['usertype']."';</script>";
// if (session_status() == PHP_SESSION_NONE) {
//     session_start();
// }
// $url = $_SERVER['REQUEST_URI']; //returns the current URL
// $parts = explode('/',$url);
// $dir = $_SERVER['SERVER_NAME'];
// for ($i = 0; $i < count($parts) - 2; $i++) {
//  $dir .= $parts[$i] . "/";
// }
?>
<!DOCTYPE html>
<html>
  <?php include("includes/html-parts/head.php");?>
  <style>
        .skin-blue .main-header .navbar {
     margin-left: 230px;
}
       
        </style>
    <!-- <script>
      BASE_URL_Rep="<?php  $dir; ?>";
      var absolutePath = function(href) {
        var link = document.createElement("a");
        link.href = href;
        return (link.protocol+"//"+link.host+link.pathname+link.search+link.hash);
      }
    </script> -->
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="hold-transition skin-blue layout-top-nav">
    <div class="wrapper">
<!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="home.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>Shoe</b>Store  </span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Shoe</b>Store</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="dist/img/avatar5.png" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
                    <p>
                      <?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    
                    <div class="">
                      <a href="logout.php" class="btn btn-default btn-lg" style="width: 100%;">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             
            </ul>
          </div>
        </nav>
      </header>
       <!-- Full Width Column -->
      <div class="content-wrapper">
        
          <!-- Content Header (Page header) -->
          <!-- Main content -->  
          <section class="content">
              <div class="col-md-2">
              <?php include "includes/views/left-bar.php"; ?>
              </div>
              <div id="content-area" class="col-md-10">          
                   
                   <div id="section-reports" class="box box-solid">
                      <div class="box-header with-border no-padding">                        
                        
                        <div class="box box-primary box-solid ">
                            <div class="box-header with-border">
                              <h3 class="box-title">Advanced Filters</h3>
                              <div class="box-tools pull-right">
                                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                              </div><!-- /.box-tools -->
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <form class="form-horizontal">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Article</label>
                                            <div class="col-sm-9">
                                                <select name="article" class="form-control">
                                                    <option value='' >All </option>
                                                    <?php 
                                                      $query = "SELECT * FROM `article`;";
                                                    	$r =[];
                                                    	$result = mysqli_query($conn, $query);
                                                    	while($row = mysqli_fetch_assoc($result)){
                                                    		echo "<option value='{$row['article_id']}' >{$row['article_no']}</option>";
                                                    	}
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Gender</label>
                                            <div class="col-sm-9">
                                                <select name="gender" class="form-control">
                                                    <option value='' >All </option>
                                                    <?php 
                                                      $query = "SELECT * FROM `gender`";
                                                  	// echo $query;
                                                  	$r =[];
                                                  	$result = mysqli_query($conn, $query);
                                                  	while($row = mysqli_fetch_assoc($result)){
                                                  		echo "<option value='{$row['gender_id']}' >{$row['gender_name']}</option>";
                                                  	}
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Color</label>
                                            <div class="col-sm-9">
                                                <select name="color" class="form-control">
                                                    <option value='' >All </option>
                                                    <?php 
                                                        $query = "SELECT * FROM `color`";
                                                    	// echo $query;
                                                    	$r =[];
                                                    	$result = mysqli_query($conn, $query);
                                                    	while($row = mysqli_fetch_assoc($result)){
                                                    		echo "<option value='{$row['color_id']}' >{$row['color_name']}</option>";
                                                    	}
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Size</label>
                                            <div class="col-sm-9">
                                                <select name="size" class="form-control">
                                                    <option value='' >All </option>
                                                    <?php 
                                                        $query = "SELECT * FROM `size`";
                                                      // echo $query;
                                                      $r =[];
                                                      $result = mysqli_query($conn, $query);
                                                      while($row = mysqli_fetch_assoc($result)){
                                                        echo "<option value='{$row['size_id']}' >{$row['size_no']}</option>";
                                                      }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-3">
                                        <div class="checkbox">
                                            <label>
                                                <input name="ins" type="checkbox"> With Installments
                                            </label>
                                        </div>
                                    </div> -->
                                   <div style="clear:both"></div>
                                    <div class="col-md-3 pull-right">
                                       
                                       <button style="margin:3px" name="print" class="btn btn-primary  pull-right">Print</button>
                                       <button style="margin:3px" name="apply" class="btn btn-primary   pull-right">Apply</button>
                                    </div>
                                    <div class="col-md-3">
                                       
                                    </div>
                                    
                                 </form>
                            </div><!-- /.box-body -->
                        </div>
                      </div><!-- /.box-header -->
                      <div name="section-content" class="box-body">
                         
                      </div><!-- /.box-body --> 
                  </div>
              </div>
          </section>        
      </div><!-- /.content-wrapper -->
    </div><!-- ./wrapper -->
    <!-- <?php include('includes/views/footer.php');?> -->
         <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
    </body>
</html>
