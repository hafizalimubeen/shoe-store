$(document).ready(function(){
	$('[name="error_msgs"]').hide();
	$('#login_err_msgs').hide();

    // check= true;
    // console.log(userSession);
    // console.log(usertype);

    $(document).on('click','[name="loginBtn"]', function(evt){
		evt.preventDefault();
		check = true;
		error_msg = "";
		
		var user = $('[id="user_name"]').val();
		var pass = $('[id="user_password"]').val();

		if(user == ''){
			check = false;
			error_msg = error_msg+"* Username cannot be empty<br>";
			$('[id="user_name"]').css('border-color','red');
		}

		if(pass == ''){
			check = false;
			error_msg = error_msg+"* Password cannot be empty<br>";
			$('[id="user_password"]').css('border-color','red');
		}

		if(check === false){
			$('[id="login_err_msgs"]').html(error_msg);
			$('[id="login_err_msgs"]').show();
		}


		if(check === true){
			$('[id="login_err_msgs"]').hide();
	    	var loginInfo = "user="+user+"&pass="+pass + "&action="+"login";
	    	$.ajax({
				type: "POST",
				url: "includes/controllers/register_controller.php",
				data: loginInfo,
				async:true,
				success: function(result){
					result=result.split('~');                                       
					if(result[1] != ''){
						$('[id="login_err_msgs"]').html(result[1]);
						$('[id="login_err_msgs"]').show();
						// alert(result[1]);
						// location.href = result[0];
					}else{                      
						location.href = result[0];	
					}		
				}
			});
			return false;
		}
	});

});