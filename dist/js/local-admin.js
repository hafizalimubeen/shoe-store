$(document).ready(function(){
	var due_amount;

	$('#sup_details').show();
	$('#article_detail').hide();
	$('#maker_add').hide();
	$('#type_add').hide();
	$('#article_error_message').hide();
	$('#merror_message').hide();
	$('#type_error_message').hide();
	$('#serror_message').hide();
    $("#addtnl-supplier-body").hide(); 


	$('#sup').click(function(){
		$('#sup_details').show();
		$('#article_detail').hide();
		$('#maker_add').hide();
		$('#type_add').hide();
		$('#article_error_message').hide();
		$('#merror_message').hide();
		$('#type_error_message').hide();
		$('#serror_message').hide();
	});

	$('#artcl').click(function(){
		$('#sup_details').hide();
		$('#article_detail').show();
		$('#maker_add').hide();
		$('#type_add').hide();
		$('#article_error_message').hide();
		$('#merror_message').hide();
		$('#type_error_message').hide();
		$('#serror_message').hide();
	});

	$('#clr').click(function(){
		$('#sup_details').hide();
		$('#article_detail').hide();
		$('#maker_add').hide();
		$('#type_add').hide();
		$('#article_error_message').hide();
		$('#merror_message').hide();
		$('#type_error_message').hide();
		$('#serror_message').hide();
	});

	$('#siz').click(function(){
		$('#sup_details').hide();
		$('#article_detail').hide();
		$('#maker_add').hide();
		$('#type_add').hide();
		$('#article_error_message').hide();
		$('#merror_message').hide();
		$('#type_error_message').hide();
		$('#serror_message').hide();
	});

	$('#maker').click(function(){
		$('#sup_details').hide();
		$('#article_detail').hide();
		$('#maker_add').show();
		$('#type_add').hide();
		$('#article_error_message').hide();
		$('#merror_message').hide();
		$('#type_error_message').hide();
		$('#serror_message').hide();
	});

	$('#typ').click(function(){
		$('#sup_details').hide();
		$('#article_detail').hide();
		$('#maker_add').hide();
		$('#type_add').show();
		$('#article_error_message').hide();
		$('#merror_message').hide();
		$('#type_error_message').hide();
		$('#serror_message').hide();
	});

    $("#addtnl-supplier").on("change",function(){
      if($('#addtnl-supplier').is(":checked")){
          $("#addtnl-supplier-body").show();
          $('#d_amnt-grp').hide();
        }
        else{
          $("#addtnl-supplier-body").hide(); 
        }
    });

    $("#p_amnt").on("focusout",function(){
    	var t_amnt = $('#t_amnt').val();
		var p_amnt = $('#p_amnt').val();
		if(t_amnt != '' && p_amnt != ''){
          $('#d_amnt-grp').show();			
          $('#d_amnt').val(parseInt(t_amnt) - parseInt(p_amnt));
		}
    });

	//Supplier Adding in admin panel
	$(document).on('click','#submit_sup', function(evt){
		evt.preventDefault();
		check = true;
		error_msg = "";
		$('#submit_sup').prop("disabled",true);
		
		var sup_name = $('#s_name').val();
		var sup_addr = $('#s_address').val();
		var sup_phn = $('#s_phone').val();
		var t_amnt = $('#t_amnt').val();
		var p_amnt = $('#p_amnt').val();
		var d_amnt = $('#d_amnt').val();
		var dt_amnt = $('#dt_amnt').val();
		$('#s_name').css('border-color','#3c8dbc');
		$('#s_address').css('border-color','#3c8dbc');
		$('#s_phone').css('border-color','#3c8dbc');
		$('#t_amnt').css('border-color','#3c8dbc');
		$('#p_amnt').css('border-color','#3c8dbc');
		$('#dt_amnt').css('border-color','#3c8dbc');

		if(sup_name == ''){
			check = false;
			error_msg = error_msg+"* Supplier name cannot be empty.<br>";
			$('#s_name').css('border-color','red');
		}

		if(sup_phn == ''){
			check = false;
			error_msg = error_msg+"* Supplier phone number is required.<br>";
			$('#s_phone').css('border-color','red');
		}else if(!sup_phn.match(/^\d{4} \d{7}$/i)){
			check=false;
			$('#s_phone').css('border-color','red');
			error_msg=error_msg+"* Phone number should be like that '0300 1234567'<br/>";
		}

		if(sup_addr == ''){
			check = false;
			error_msg = error_msg+"* Supplier address is required.<br>";
			$('#s_address').css('border-color','red');
		}

		if($('#addtnl-supplier').is(":checked")){
			if(t_amnt == ''){
				check = false;
				error_msg = error_msg+"* Total amount is required.<br>";
				$('#t_amnt').css('border-color','red');
			}else if(t_amnt.length >0){                     
				if(!($.isNumeric(t_amnt))){
					check=false;
					error_msg= error_msg+"* Please Enter Proper Amount in Rs<br/>";
					$('#t_amnt').css('border-color','red');
				}
			}

			if(p_amnt == ''){
				check = false;
				error_msg = error_msg+"* Paid Amount is required.<br>";
				$('#p_amnt').css('border-color','red');
          		$('#d_amnt-grp').hide();

			}else if(p_amnt.length >0){                     
				if(!($.isNumeric(p_amnt))){
					check=false;
					error_msg= error_msg+"* Please Enter Proper Paid Amount in Rs<br/>";
					$('#p_amnt').css('border-color','red');
          			$('#d_amnt-grp').hide();
				}else if(parseInt(t_amnt) < parseInt(p_amnt)){
					check=false;
					error_msg= error_msg+"* Paid amount cannot be greater than total amount.<br/>";
					$('#p_amnt').css('border-color','red');
	          		$('#d_amnt-grp').hide();
				}
			}

			if(dt_amnt == ''){
				check = false;
				error_msg = error_msg+"* Please select a date at which amount is paid.<br>";
				$('#dt_amnt').css('border-color','red');
			}

			due_amount = parseInt(t_amnt) - parseInt(p_amnt);
			// console.log(due_amount);
			console.log(due_amount);

		}

		if(check === false){
			$('#serror_message').html(error_msg);
			$('#serror_message').show();
			$('#submit_sup').prop("disabled",false);
		}

		if(check === true){
			if(due_amount == undefined){
				due_amount = "";
				console.log("due = "+due_amount);
			}
			$('#serror_message').hide();
         	$('#d_amnt-grp').show();
	    	var supplierInfo = "sup_name="+sup_name+"&sup_phn="+sup_phn+"&sup_addr="+sup_addr+"&t_amnt="+t_amnt+
	    	"&p_amnt="+p_amnt+"&d_amnt="+due_amount+"&dt_amnt="+dt_amnt+"&userid="+userid+"&oper="+"addingSupplier";
	    	// console.log(supplierInfo);
	    	$.ajax({
				type: "POST",
				url: "includes/controllers/admin_controller.php",
				data: supplierInfo,
				success: function(result){ 
					result = result.split('~');                                      
					if(result[1] == 'OK'){
						alert("Supplier '"+result[0]+"' has been added.");
						$('#s_name').val('');
						$('#s_address').val('');
						$('#s_phone').val('');
						$('#t_amnt').val('');
						$('#p_amnt').val('');
						$('#d_amnt').val('');
						$('#dt_amnt').val('');
						$('#s_name').css('border-color','#d2d6de');
						$('#s_address').css('border-color','#d2d6de');
						$('#s_phone').css('border-color','#d2d6de');
						$('#t_amnt').css('border-color','#d2d6de');
						$('#p_amnt').css('border-color','#d2d6de');
						$('#dt_amnt').css('border-color','#d2d6de');
						$('#d_phone').css('border-color','#d2d6de');
			         	$('#d_amnt-grp').hide();
				        $("#addtnl-supplier-body").hide();
				        $( "#addtnl-supplier" ).prop( "checked", false );
					}else{
						alert("Supplier is not added, please try again");
					}
					$('#submit_sup').prop("disabled",false);	
				}
			});
			return false;
		}
	});

	//Adding Article in admin
	$(document).on('click','#submit_article', function(evt){
		evt.preventDefault();
		check = true;
		error_msg = "";
		$('#submit_article').prop("disabled",true);
		
		var artcl_dtl = $('#article-dtl').val();
		$('#article-dtl').css('border-color','#3c8dbc');

		var clr_dtl = $('#color-dtl').val();
		$('#color-dtl').css('border-color','#3c8dbc');

		var size = $('#size_dtl').val();
		var size_dtl= $("#size_dtl option[value="+size+"]").text();

		var gender = $('#gender_dtl').val();
		var gender_dtl= $("#gender_dtl option[value="+gender+"]").text();

		if(artcl_dtl == ''){
			check = false;
			error_msg = error_msg+"* Article name cannot be empty.<br>";
			$('#article-dtl').css('border-color','red');
		}

		if(size == null){
			check = false;
			error_msg = error_msg+"* Please select a size for the shoe.<br>";
			$('#size_dtl').css('border-color','red');
		}

		if(clr_dtl == ''){
			check = false;
			error_msg = error_msg+"* Color name cannot be empty.<br>";
			$('#color-dtl').css('border-color','red');
		}

		if(gender == null){
			check = false;
			error_msg = error_msg+"* Please select a gender.<br>";
			$('#gender_dtl').css('border-color','red');
		}

		if(check === false){
			$('#article_error_message').html(error_msg);
			$('#article_error_message').show();
			$('#submit_article').prop("disabled",false);
		}

		if(check === true){
			var artile_title = artcl_dtl +"-"+size_dtl+"-"+clr_dtl+"-"+gender_dtl;
			$('#article_error_message').hide();
	    	var articleInfo = "artcl_dtl="+artcl_dtl+"&artile_title="+artile_title+"&size_dtl="+size_dtl+
			"&clr_dtl="+clr_dtl+"&gender_dtl="+gender_dtl+"&userid="+userid+"&oper="+"addingArticle";
	    	$.ajax({
				type: "POST",
				url: "includes/controllers/admin_controller.php",
				data: articleInfo,
				success: function(result){                                       
					if(result == 'Error'){
						alert("Article is not added, please try again");
					}else{
						alert(result);
						$('#article-dtl').val('');
						$('#color-dtl').val('');
						$('#size_dtl').val(null);
						$('#gender_dtl').val(null);
						$('#article-dtl').css('border-color','#d2d6de');
						$('#color-dtl').css('border-color','#d2d6de');
						$('#size_dtl').css('border-color','#d2d6de');
						$('#gender_dtl').css('border-color','#d2d6de');
					}
					$('#submit_article').prop("disabled",false);	
				}
			});
			return false;
		}
	});

	//Adding Maker in Admin
	$(document).on('click','#submit_maker', function(evt){
		evt.preventDefault();
		check = true;
		error_msg = "";
		$('#submit_maker').prop("disabled",true);
		
		var maker_name = $('#maker_name').val();
		$('#maker_name').css('border-color','#3c8dbc');

		if(maker_name == ''){
			check = false;
			error_msg = error_msg+"* Maker name cannot be empty.<br>";
			$('#maker_name').css('border-color','red');
		}

		if(check === false){
			$('#merror_message').html(error_msg);
			$('#merror_message').show();
			$('#submit_maker').prop("disabled",false);
		}

		if(check === true){
			$('#merror_message').hide();
	    	var makerInfo = "maker_name="+maker_name+"&userid="+userid+"&oper="+"addingMaker";
	    	$.ajax({
				type: "POST",
				url: "includes/controllers/admin_controller.php",
				data: makerInfo,
				success: function(result){                                       
					if(result == 'Error'){
						alert("Color is not added, please try again");
					}else{
						alert(result);
						$('#maker_name').val('');
						$('#maker_name').css('border-color','#d2d6de');
					}
					$('#submit_maker').prop("disabled",false);	
				}
			});
			return false;
		}
	});

	//Adding Type in Admin
	$(document).on('click','#submit_type', function(evt){
		evt.preventDefault();
		check = true;
		error_msg = "";
		$('#submit_type').prop("disabled",true);
		
		var type_name = $('#type_name').val();
		$('#type_name').css('border-color','#3c8dbc');

		if(type_name == ''){
			check = false;
			error_msg = error_msg+"* Type name cannot be empty.<br>";
			$('#type_name').css('border-color','red');
		}

		if(check === false){
			$('#type_error_message').html(error_msg);
			$('#type_error_message').show();
			$('#submit_type').prop("disabled",false);
		}

		if(check === true){
			$('#type_error_message').hide();
	    	var typeInfo = "type_name="+type_name+"&userid="+userid+"&oper="+"addingType";
	    	$.ajax({
				type: "POST",
				url: "includes/controllers/admin_controller.php",
				data: typeInfo,
				success: function(result){                                       
					if(result == 'Error'){
						alert("Color is not added, please try again");
					}else{
						alert(result);
						$('#type_name').val('');
						$('#type_name').css('border-color','#d2d6de');
					}
					$('#submit_type').prop("disabled",false);	
				}
			});
			return false;
		}
	});

});