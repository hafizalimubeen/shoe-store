$(document).ready(function(){
	$('#purchase_order').show();
	$('#adding_order').hide();
	$('#order_error_message').hide();
	$('#order_detail_error_message').hide();
	$('#shoe-specs').hide();
	$('#product-dtl-div').hide();

	loadSuppliers();
	loadMakers();
	loadTypes();
	loadArticles();
	loadSuppliersForDetail()
	// loadSizes();
	// loadColors();
	// loadGender();

	// $('#order_detail_article').dropdown();
	

	$(document).on('click','#submit_order_detail',function(evt){
		evt.preventDefault();
		check = true;
		error_msg = "";
		$('#submit_order_detail').prop("disabled",true);
		
		var sup = $('#order_supplier').val();
		var supplier_name= $("#order_supplier option[value="+sup+"]").text();
		// $( "#order_maker option:selected" ).text();
		var quantity = $('#order_quan').val();
		var supplier_id =$("#order_supplier option[value="+sup+"]").attr('id');
		$('#order_supplier').css('border-color','#3c8dbc');
		$('#order_quan').css('border-color','#3c8dbc');

		if(sup === null){
			check = false;
			error_msg = error_msg+"* Please select a supplier from the list.<br>";
			$('#order_supplier').css('border-color','red');
		}

		if(quantity.length<=0){
			 check=false;
			 error_msg= error_msg+"* Enter Quantity to be Added<br/>";
			 $('#order_quan').css('border-color','red');
		}

		if(quantity.length >0){                     
			if(!($.isNumeric(quantity))){
				check=false;
				error_msg= error_msg+"* Please Enter Proper Quantity<br/>";
				$('#order_quan').css('border-color','red');
			}
		}

		if(check === false){
			$('#order_error_message').html(error_msg);
			$('#order_error_message').show();
			$('#submit_order_detail').prop("disabled",false);

		}

		if(check === true){
			$('#order_error_message').hide();
	    	var typeInfo = "userid="+userid+"&supplier_id="+supplier_id+"&action="+"addPurchase";
	    	$.ajax({
				type: "POST",
				url: "includes/controllers/inventory_controller.php",
				data: typeInfo,
				success: function(result){ 				
	                $('#purchase_order').hide();
	                $('#adding_order').show();                                                                    
	                total_quan= $('#order_quan').val();
	                var po_id= result;
                    id=po_id;
					$('#order_supplier').chosen("destroy");
                    $('#order_quan').val('');
                    startAdding(total_quan,'first');                                      
					// alert(result);
					$('#submit_order_detail').prop("disabled",false);
					loadSuppliers();	
				}
			});
			return false;
		}
	});


	$(document).on('click','#submit_product_detail',function(evt){
		evt.preventDefault();
		check = true;
		error_msg = "";
		$('#submit_product_detail').prop("disabled",true);
		var maker_val = $('#order_maker').val();
		var makerid= $("#order_maker option[value="+maker_val+"]").attr('id');

		var type_val = $('#order_detail_type').val();
		var typeid= $("#order_detail_type option[value="+type_val+"]").attr('id');

		var artcl_val = $('#order_detail_article').val();
		var articleid= $("#order_detail_article option[value="+artcl_val+"]").attr('id');

		// var size_val = $('#order_detail_size').val();
		// var sizeid= $("#order_detail_size option[value="+size_val+"]").attr('id');

		// var clr_val = $('#order_detail_color').val();
		// var colorid= $("#order_detail_color option[value="+clr_val+"]").attr('id');

		// var gender_val = $('#order_detail_gender').val();
		// var genderid= $("#order_detail_gender option[value="+gender_val+"]").attr('id');

		var article_number = $('#order_detail_article_no').val();
		var size_val = $('#order_detail_size').val();
		var clr_val = $('#order_detail_color').val();
		var gender_val = $('#order_detail_gender').val();

		var another_name = $('#another_name').val();
		var pr_price = $('#order_price').val();
		var shoes_quantity = $('#order_quantity').val();



		$('#order_maker').css('border-color','#3c8dbc');
		$('#order_detail_type').css('border-color','#3c8dbc');
		$('#order_detail_article').css('border-color','#3c8dbc');
		// $('#order_detail_size').css('border-color','#3c8dbc');
		// $('#order_detail_color').css('border-color','#3c8dbc');
		// $('#order_detail_gender').css('border-color','#3c8dbc');
		$('#order_price').css('border-color','#3c8dbc');
		$('#order_quantity').css('border-color','#3c8dbc');

		if(maker_val == null){
			check = false;
			error_msg = error_msg+"* Please select a Maker from the list.<br>";
			$('#order_maker').css('border-color','red');
		}

		if(type_val == null){
			check = false;
			error_msg = error_msg+"* Please select a type from the list.<br>";
			$('#order_detail_type').css('border-color','red');
		}

		if(artcl_val == null){
			check = false;
			error_msg = error_msg+"* Please select an Article from the list.<br>";
			$('#order_detail_article').css('border-color','red');
		}

		if(pr_price.length<=0){
			 check=false;
			 error_msg= error_msg+"* Enter Purchase Price to be Added<br/>";
			 $('#order_price').css('border-color','red');
		}

		if(pr_price.length >0){                     
			if(!($.isNumeric(pr_price))){
				check=false;
				error_msg= error_msg+"* Please Enter Proper Price<br/>";
				$('#order_price').css('border-color','red');
			}
		}

		if(shoes_quantity.length<=0){
			 check=false;
			 error_msg= error_msg+"* Enter Quantity to be Added<br/>";
			 $('#order_quantity').css('border-color','red');
		}

		if(shoes_quantity.length >0){                     
			if(!($.isNumeric(shoes_quantity))){
				check=false;
				error_msg= error_msg+"* Please Enter Proper Quantity<br/>";
				$('#order_quantity').css('border-color','red');
			}
		}

		if(check === false){
			$('#order_detail_error_message').html(error_msg);
			$('#order_detail_error_message').show();
			$('#submit_product_detail').prop("disabled",false);
		}

		// console.log(artcl_val +"  .... "+ articleid);
		if(check === true){
			$('#order_detail_error_message').hide();
	    	var typeInfo = "po_id="+ id+ "&userid="+userid+"&makerid="+makerid+"&typeid="+typeid+"&article_number="+article_number+"&articleid="+articleid+"&size_val="+size_val+"&clr_val="+clr_val 	
	    	+"&gender_val="+gender_val+"&another_name="+another_name+"&pr_price="+pr_price+"&shoes_quantity="+shoes_quantity+"&action="+"addProduct";
	    	$.ajax({
				type: "POST",
				url: "includes/controllers/inventory_controller.php",
				data: typeInfo,
				success: function(result){
					if(result == "ERROR"){
						$('#submit_product_detail').prop("disabled",false);
						alert("Please Try Again, Something went wrong with the system.");
					}else{
						alert("Saved Successfully");
						$('#shoe-specs').hide();
						$('#order_maker').chosen("destroy");
						$('#order_detail_type').chosen("destroy");
						$('#order_detail_article').chosen("destroy");
						$('#order_detail_article_no').val('');
						$('#order_detail_size').val('');
						$('#order_detail_color').val('');
						$('#order_detail_gender').val('');
						$('#another_name').val('');
						$('#order_price').val('');
						$('#order_quantity').val('');
						$('#order_maker').css('border-color','#d2d6de');
						$('#order_detail_type').css('border-color','#d2d6de');
						$('#order_detail_article').css('border-color','#d2d6de');
						$('#order_detail_size').css('border-color','#d2d6de');
						$('#order_detail_color').css('border-color','#d2d6de');
						$('#order_detail_gender').css('border-color','#d2d6de');
						$('#order_price').css('border-color','#d2d6de');
						$('#order_quantity').css('border-color','#d2d6de');
					    startAdding(total_quan,"next");			                                      
						$('#submit_product_detail').prop("disabled",false);
						loadMakers();
						loadTypes();
						loadArticles();
					}	
				}
			});
			return false;
		}
	});

	////////Adding specifications of shoes/////////////////
	$('#order_detail_article').on('change',function(evt){
		evt.preventDefault();
		evt = evt;
		clicked = $(evt.target);

		var artcl = $('#order_detail_article').val();
		var artcl_titl_id= $("#order_detail_article option[value="+artcl+"]").attr('id');
		$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=articleDetail",
			data:{'article_id':artcl_titl_id,'userid':userid},
			dataType:'json',
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Article data may not be availible.");
				}else{
					$('#shoe-specs').show();
		            for (var i = 0; i < data.length; i++) {
						$('#order_detail_article_no').val(data[i].article_no);					
						$('#order_detail_size').val(data[i].size_no);					
						$('#order_detail_color').val(data[i].color_name);					
						$('#order_detail_gender').val(data[i].gender_name);
					}					
				}	
			}
		});
		return false;
	});

	///////////////// START ADDING//////////////////////////////////////////////////
	function startAdding(quantity,action)
	{    
	    var done = $('#done').html();
	    done = parseInt(done);    
	    if(action==='first')
	    {
	       $('#done').html(done);
	       $('#remaining').html('');
	       $('#remaining').html(quantity);
	    }  
	     if(action==="next")
	     {
	         done = done + 1;         
	         var remaining= parseInt(quantity)-done;
	         if(remaining<=0)
	         {
	            $('#done_order').trigger('click');
	         }
	         if(remaining>0)
	         {
	           $('#remaining').html('');
	           $('#done').html('');
	           $('#remaining').html(remaining);
	           $('#done').html(done);
	        }  
	     }
	}
	////////////////////////////////////////////////////////////////////////////////

	//////////// Order Done/////////////////////////////////////////////////////////
	$('#done_order').on('click',function(){
	    $('#order_maker').val('none');
	    $('#order_detail_model').val('none');
	    $('#order_detail_color').val('none');
	    $('#chasey_no').val('');
	    $('#engine_no').val('');
	    $('#order_price').val('');
	    id='';
	    $('#done').html(0);
	    $('#adding_order').hide();
	    $('#purchase_order').show();
	});
	////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////
	////Purchase Order Detail Section//////////////
	///////////////////////////////////////////////
	$('#supplier-name').on('change',function(evt){
		evt.preventDefault();
		evt = evt;
		clicked = $(evt.target);

		var spl = $('#supplier-name').val();
		var spl_id = $("#supplier-name option[value="+spl+"]").attr('id');
		// console.log(spl_id);
		$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=purchaseOrderDetail",
			data:{'suppl_id':spl_id,'userid':userid},
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Information related to the supplier is not availible");
				}else{
					$('#table_body_product').html(data);
					$('#product-dtl-div').show();
					eval(`$('#ed-product-dtl').DataTable();`);
				}	
			}
		});
		return false;
	});

	//////Delete related product to supplier with purchase order///
	$(document).on('click','[name="dlt-btn-sup-prdct"]', function(evt){
		evt.preventDefault();
		evt = evt;
		clicked = $(evt.target);

		if(clicked.prop('tagName')=="I"){
            clicked=clicked.parent();
        }

		var p_id = clicked.parent().parent().attr('id');
		var pod_id = clicked.parent().parent().attr('pod-id');
		// console.log(p_id + "  :  "+pod_id);

		if (confirm("Are you sure want to delete this product, it will also delete the purchase order detail related to this product?")) {       
            $.ajax({
                type: "POST",
                url: "includes/controllers/inventory_controller.php?oper=deleteProductandPurchaseOrder",
                data: {"p_id":p_id,"pod_id":pod_id,'userid':userid},
                success: function(result){
                    if(result == "OK"){
                    	clicked.parent().parent().remove();
                    }else{
                    	alert("Please try again, some error occured");
                    }
                }
            });
            return false;
        }
        return false;
	});

	///////////////////////////////////////////////

	function loadSuppliers(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getSuppliers",
			data:{'userid':userid},
			dataType:'json',
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Supplier data may not be availible.");
				}else{
					var suppliers = $('#order_supplier');
		            suppliers.empty();
		            suppliers.append('<option value="" disabled selected>Please Select a Supplier</option>');
		            for (var i = 0; i < data.length; i++) {
		                suppliers.append("<option id=" + data[i].sup_id+" value=" + data[i].sup_name+">" + data[i].sup_name + "</option>");
		            }
					suppliers.chosen({width:"100%"});
				}	
			}
		});
		return false;
	}

	function loadSuppliersForDetail(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getSuppliers",
			data:{'userid':userid},
			dataType:'json',
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Supplier data may not be availible.");
				}else{
					var suppliers = $('#supplier-name');
		            suppliers.empty();
		            suppliers.append('<option value="" disabled selected>Please Select a Supplier</option>');
		            for (var i = 0; i < data.length; i++) {
		                suppliers.append("<option id=" + data[i].sup_id+" value=" + data[i].sup_name+">" + data[i].sup_name + "</option>");
		            }
					suppliers.chosen({width:"100%"});
				}	
			}
		});
		return false;
	}

	function loadMakers(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getMakers",
			data:{'userid':userid},
			dataType:'json',
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Makers data may not be availible.");
				}else{
					var makers = $('#order_maker');
		            makers.empty();
		            makers.append('<option value="" disabled selected>Please Select a Maker</option>');
		            for (var i = 0; i < data.length; i++) {
		                makers.append("<option id=" + data[i].maker_id+" value=" + data[i].maker_name+">" + data[i].maker_name + "</option>");
		            }
					makers.chosen({width:"100%"});
				}	
			}
		});
		return false;
	}

	function loadTypes(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getTypes",
			data:{'userid':userid},
			dataType:'json',
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Types data may not be availible.");
				}else{
					var types = $('#order_detail_type');
		            types.empty();
		            types.append('<option value="" disabled selected>Please Select a Type</option>');
		            for (var i = 0; i < data.length; i++) {
		                types.append("<option id=" + data[i].type_id+" value=" + data[i].type_name+">" + data[i].type_name + "</option>");
		            }
					types.chosen({width:"100%"});
				}	
			}
		});
		return false;
	}

	function loadArticles(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getArticles",
			data:{'userid':userid},
			dataType:'json',
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Articles data may not be availible.");
				}else{
					var articles = $('#order_detail_article');
		            articles.empty();
		            articles.append('<option value="" disabled selected>Please Select an Article</option>');
		            for (var i = 0; i < data.length; i++) {
		                articles.append("<option id=" + data[i].article_id+" value=" + data[i].article_title+">" + data[i].article_title + "</option>");
		            }
					articles.chosen({width:"100%"});
				}	
			}
		});
		return false;
	}

	// function loadSizes(){
 //    	$.ajax({
	// 		type: "GET",
	// 		url: "includes/controllers/inventory_controller.php?oper=getSizes",
	// 		dataType:'json',
	//         // async: true,
	// 		success: function(data){                                       
	// 			if(data == 'Error'){
	// 				alert("Sizes data may not be availible.");
	// 			}else{
	// 				var sizes = $('#order_detail_size');
	// 	            sizes.empty();
	// 	            sizes.append('<option value="" disabled selected>Please Select a Size</option>');
	// 	            for (var i = 0; i < data.length; i++) {
	// 	                sizes.append("<option id=" + data[i].size_id+" value=" + data[i].size_no+">" + data[i].size_no + "</option>");
	// 	            }
	// 			}	
	// 		}
	// 	});
	// 	return false;
	// }

	// function loadColors(){
 //    	$.ajax({
	// 		type: "GET",
	// 		url: "includes/controllers/inventory_controller.php?oper=getColors",
	// 		dataType:'json',
	//         // async: true,
	// 		success: function(data){                                       
	// 			if(data == 'Error'){
	// 				alert("Colors data may not be availible.");
	// 			}else{
	// 				var colors = $('#order_detail_color');
	// 	            colors.empty();
	// 	            colors.append('<option value="" disabled selected>Please Select a Color</option>');
	// 	            for (var i = 0; i < data.length; i++) {
	// 	                colors.append("<option id=" + data[i].color_id+" value=" + data[i].color_name+">" + data[i].color_name + "</option>");
	// 	            }
	// 			}	
	// 		}
	// 	});
	// 	return false;
	// }

	// function loadGender(){
 //    	$.ajax({
	// 		type: "GET",
	// 		url: "includes/controllers/inventory_controller.php?oper=getGender",
	// 		dataType:'json',
	//         // async: true,
	// 		success: function(data){                                       
	// 			if(data == 'Error'){
	// 				alert("Gender data may not be availible,please reload.");
	// 			}else{
	// 				var gender = $('#order_detail_gender');
	// 	            gender.empty();
	// 	            gender.append('<option value="" disabled selected>Please Select a Gender</option>');
	// 	            for (var i = 0; i < data.length; i++) {
	// 	                gender.append("<option id=" + data[i].gender_id+" value=" + data[i].gender_name+">" + data[i].gender_name + "</option>");
	// 	            }
	// 			}	
	// 		}
	// 	});
	// 	return false;
	// }

});