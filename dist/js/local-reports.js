last_report="rpt-customer";
$(document).on('click','[name="rpt-customer"]',function(evt){
    evt.preventDefault();
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/customer.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#customers').DataTable();`);
    });
    return false;
});

$(document).on('click','[name="rpt-payments"]',function(evt){
    evt.preventDefault();
    // $('[name="year"]').parent().parent().hide();
    // $('[name="article"]').parent().parent().hide();
    // $('[name="power"]').parent().parent().hide();
    // $('[name="type"]').parent().parent().hide();
    // $('[name="ins"]').parent().parent().hide();
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/payments.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#payments').DataTable();`);
    });
    return false;
});

$(document).on('click','[name="rpt-installments"]',function(evt){
    evt.preventDefault();
    // $('[name="year"]').parent().parent().hide();
    // $('[name="article"]').parent().parent().hide();
    // $('[name="power"]').parent().parent().hide();
    // $('[name="type"]').parent().parent().hide();
    // $('[name="ins"]').parent().parent().hide();
    
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/installments.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#installments').DataTable();`);
    });
    return false;
});

$(document).on('click','[name="rpt-sales"]',function(evt){
    evt.preventDefault();
    $('[name="article"]').parent().parent().show();
    $('[name="gender"]').parent().parent().show();
    $('[name="color"]').parent().parent().show();
    $('[name="size"]').parent().parent().show();
    // console.log(userid);
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/sales.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#sales').DataTable();`);
    });
    return false;
});

// $(document).on('click','[name="rpt-sales-d"]',function(evt){
//     evt.preventDefault();
//     $('[name="year"]').parent().parent().show();
//     $('[name="article"]').parent().parent().show();
//     $('[name="power"]').parent().parent().show();
//     $('[name="type"]').parent().parent().hide();
//     $('[name="ins"]').parent().parent().show();
    
//     fil=$("form").serialize();
//     last_report=$(evt.target).attr("name");
//     $.post('includes/reports/sales.php',fil,function(res){
//         $('#section-reports [name="section-content"]').html(res);
//         eval(`$('#sales').DataTable();`);});
//     return false;
// });

$(document).on('click','[name="rpt-profit-loss"]',function(evt){
    evt.preventDefault();
    $('[name="article"]').parent().parent().show();
    $('[name="gender"]').parent().parent().show();
    $('[name="color"]').parent().parent().show();
    $('[name="size"]').parent().parent().show();
    
    
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/profit-loss.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#profit-loss').DataTable();`);
    });
    return false;
});
$(document).on('click','[name="btn-del-cust"]',function(evt){
    evt.preventDefault();
    target1=$(evt.target).prop("tagName");
    if(target1=='I')target1=$(evt.target).parent();
    else target1=$(evt.target);
    target=target1.attr('target');
    console.log(target);  
    var res=confirm("Do you Want To Delete Selected Customer?");
    if(res)
        $.post('includes/delete-cust.php?target='+target,{target:target},function(res){
            alert(res);
            target1.parent().parent().remove(); 
        });
    return false;
});
$(document).on('click','[name="rpt-summary"]',function(evt){
    evt.preventDefault();
    $('[name="article"]').parent().parent().hide();
    $('[name="gender"]').parent().parent().hide();
    $('[name="color"]').parent().parent().hide();
    $('[name="size"]').parent().parent().hide();
    
    
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/summary.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#summary').DataTable();
    $('#mk-summary').DataTable();`);
    });
    return false;
});

$(document).on('click','[name="rpt-summary-2"]',function(evt){
    evt.preventDefault();
    $('[name="article"]').parent().parent().hide();
    $('[name="gender"]').parent().parent().hide();
    $('[name="color"]').parent().parent().hide();
    $('[name="size"]').parent().parent().hide();
    
    
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/summary-2.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#summary-2').DataTable();
    $('#mk-summary-2').DataTable();`);
    });
    return false;
});

$(document).on('click','[name="rpt-inventory"]',function(evt){
    evt.preventDefault();
    $('[name="article"]').parent().parent().hide();
    $('[name="gender"]').parent().parent().hide();
    $('[name="color"]').parent().parent().hide();
    $('[name="size"]').parent().parent().hide();
    
    
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/inventory.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#inventory').DataTable();`);
    });
    return false;
    // {
    //     "pageLength": 5 
    // }
});

$(document).on('click','[name="rpt-other-expense"]',function(evt){
    evt.preventDefault();
    $('[name="article"]').parent().parent().hide();
    $('[name="gender"]').parent().parent().hide();
    $('[name="color"]').parent().parent().hide();
    $('[name="size"]').parent().parent().hide();
    
    
    fil=$("form").serialize();
    last_report=$(evt.target).attr("name");
    $.post('includes/reports/extra-exp.php?user_id='+userid,fil,function(res){
        $('#section-reports [name="section-content"]').html(res);
        eval(`
    $('#extra-expense').DataTable();`);
    });
    return false;
    // {
    //     "pageLength": 5 
    // }
});


$(document).on('click','[name="apply"]',function(evt){
    evt.preventDefault();
    $("[name='"+last_report+"']").trigger("click");
    return false;
});
$(document).on('click','[name="print"]',function(evt){
    evt.preventDefault();
    arr=$(document).find('link');
str="";
for(i=0;i<arr.length;i++){
href=$(arr[i]).attr("href");

p=absolutePath(href);
str+=`<link rel="stylesheet" href="`+p+`">`;
}
    doc_head=str;
    head=$("[name='section-content'] h3").html();
    content="";
    arr=$("[name='section-content'] table");
    for(i=0;i<arr.length;i++){
        content+="<table class='table table-bordered table-striped'>"+$(arr[i]).html()+"</table><br>";
    }
    var w = window.open();
    img=absolutePath('dist/img/h.jpg'); 
    html="<head>"+doc_head+"</head><img style=' width: 100%; height: 130px;' src='"+img +"'><h4 class='text-center' >"+head+"</h4>"+content+"<h4 class='text-center' >Report Generated Using <a>Automatia</a></h4>"
    $(w.document.body).html(html);
    return false;
});

$(document).ready(function(){
    // setInterval(function(){
    //     $.post("includes/notif.php",{},function(res){
    //         data=JSON.parse(res);
    //         keys=Object.keys(data);
    //         for(i=0;i<keys.length;i++){
    //             html=html_notif;
    //             k=keys[i];
    //            html= html.replace("#acc",data[k]['account_id']);
    //            html= html.replace("#acc",data[k]['account_id']);
    //             html= html.replace("#amount",data[k]['amount']);
    //              html= html.replace("#cust",data[k]['name']);
    //              if($("#"+data[k]['account_id']).length<=0)
    //             $("#notify-ul").append(html);
    //         }
    //     });
    // },3000);
});