$(document).ready(function(){
	// $('#ed-article-dtl_wrapper').hide();
	// $('#ed-maker-dtl_wrapper').hide();
	// $('#ed-type-dtl_wrapper').hide();
	$('#ed-supplier-dtl').show();
	$('#ed-article-dtl').hide();
	$('#ed-maker-dtl').hide();
	$('#ed-type-dtl').hide();


    // $('#ed-sup').trigger('click',function(){$('#ed-article-dtl_wrapper').hide()});


	$('#ed-sup').click(function(){
		$('#ed-supplier-dtl').show();
		$('#ed-article-dtl').hide();
		$('#ed-maker-dtl').hide();
		$('#ed-type-dtl').hide();
		$('#ed-supplier-dtl_wrapper').show();
		$('#ed-article-dtl_wrapper').hide();
		$('#ed-maker-dtl_wrapper').hide();
		$('#ed-type-dtl_wrapper').hide();
	});

	$('#ed-artcl').click(function(){
		$('#ed-supplier-dtl').hide();
		$('#ed-article-dtl').show();
		$('#ed-maker-dtl').hide();
		$('#ed-type-dtl').hide();
		$('#ed-supplier-dtl_wrapper').hide();
		$('#ed-article-dtl_wrapper').show();
		$('#ed-maker-dtl_wrapper').hide();
		$('#ed-type-dtl_wrapper').hide();
	});

	$('#ed-maker').click(function(){
		$('#ed-supplier-dtl').hide();
		$('#ed-article-dtl').hide();
		$('#ed-maker-dtl').show();
		$('#ed-type-dtl').hide();
		$('#ed-supplier-dtl_wrapper').hide();
		$('#ed-article-dtl_wrapper').hide();
		$('#ed-maker-dtl_wrapper').show();
		$('#ed-type-dtl_wrapper').hide();
	});

	$('#ed-typ').click(function(){
		$('#ed-supplier-dtl').hide();
		$('#ed-article-dtl').hide();
		$('#ed-maker-dtl').hide();
		$('#ed-type-dtl').show();
		$('#ed-supplier-dtl_wrapper').hide();
		$('#ed-article-dtl_wrapper').hide();
		$('#ed-maker-dtl_wrapper').hide();
		$('#ed-type-dtl_wrapper').show();
	});

	$(document).on("click","[name='btn-row-edit']",function(res){
		var t=$(res.target);
		if(t.prop('tagName')=="I"){
		 t=t.parent();
		}
		t.parent().parent().addClass('edit');
		t.hide();
	});

	// $('#edit_supplier').on( 'click', 'tbody td', function () {
	//     edit_supplier.cell( this ).edit();
	// } );


	loadSuppliers();
	loadMakers();
	loadTypes();
	loadArticles();


	///////////Updating or removing suppliers information////////////////
	//Remove a supplier
    $(document).on("click",'[name="dlt-btn-sup"]',function(evt){
        evt.preventDefault();
        evt =evt;
        clicked = $(evt.target);
        if(clicked.prop('tagName')=="I"){
             clicked=clicked.parent();
         }
        var supplierid= clicked.parent().parent().attr('id');

        if (confirm("Are you sure want to delete Supplier?")) {       
            $.ajax({
                type: "POST",
                url: "includes/controllers/admin_controller.php?oper=deleteSupplier",
                data: "supplierid="+supplierid+"&userid="+userid,
                success: function(result){
                    if(result == "OK"){
                    	clicked.parent().parent().remove();
                    }else{
                    	alert("Please try again, some error occured");
                    }
                }
            });
            return false;
        }
        return false;
    });

    //Updating Supplier information.
    $(document).on("click",'.sup-upd-save',function(evt){
        evt.preventDefault();
        evt=evt;
        clicked= $(evt.target);
		check = true;
        error_msg = "";
        
        if(clicked.prop('tagName')=="I"){
            clicked=clicked.parent();
        }

        var sup_id = clicked.parent().parent().attr('id');
        var sup_name =clicked.parent().parent().find('[name="sup_name"]').val();
        var sup_phone =clicked.parent().parent().find('[name="sup_phone"]').val();
        var sup_address =clicked.parent().parent().find('[name="sup_address"]').val();
        var p_amnt =clicked.parent().parent().find('[name="p_amnt"]').val();
        var dt_amnt =clicked.parent().parent().find('[name="dt_amnt"]').val();
        var t_amnt =clicked.parent().parent().find('[name="t_amnt"]').val();
        var d_amnt = parseInt(t_amnt) - parseInt(p_amnt);

        clicked.parent().parent().find('[name="sup_name"]').css('border-color','#3c8dbc');
        clicked.parent().parent().find('[name="sup_phone"]').css('border-color','#3c8dbc');
        clicked.parent().parent().find('[name="sup_address"]').css('border-color','#3c8dbc');
        clicked.parent().parent().find('[name="p_amnt"]').css('border-color','#3c8dbc');
        clicked.parent().parent().find('[name="dt_amnt"]').css('border-color','#3c8dbc');
        clicked.parent().parent().find('[name="t_amnt"]').css('border-color','#3c8dbc');

        if(sup_name == ''){
			check = false;
			error_msg = error_msg+"* Supplier name cannot be empty.\n";
			clicked.parent().parent().find('[name="sup_name"]').css('border-color','red');
		}

		if(sup_phone == ''){
			check = false;
			error_msg = error_msg+"* Supplier phone number is required.\n";
			clicked.parent().parent().find('[name="sup_phone"]').css('border-color','red');
		}else if(!sup_phone.match(/^\d{4} \d{7}$/i)){
			check=false;
			clicked.parent().parent().find('[name="sup_phone"]').css('border-color','red');
			error_msg=error_msg+"* Phone number should be like that '0300 1234567'\n";
		}

		if(sup_address == ''){
			check = false;
			error_msg = error_msg+"* Supplier address is required.\n";
			clicked.parent().parent().find('[name="sup_address"]').css('border-color','red');
		}

        if(t_amnt == ''){
            check = false;
            error_msg = error_msg+"* Total amount is required.\n";
            clicked.parent().parent().find('[name="t_amnt"]').css('border-color','red');
        }else if(t_amnt.length >0){                     
            if(!($.isNumeric(t_amnt))){
                check=false;
                error_msg= error_msg+"* Please Enter Proper Amount in Rs\n";
                clicked.parent().parent().find('[name="t_amnt"]').css('border-color','red');
            }
        }

        if(p_amnt == ''){
            check = false;
            error_msg = error_msg+"* Paid Amount is required.\n";
            clicked.parent().parent().find('[name="p_amnt"]').css('border-color','red');
            $('#d_amnt-grp').hide();

        }else if(p_amnt.length >0){                     
            if(!($.isNumeric(p_amnt))){
                check=false;
                error_msg= error_msg+"* Please Enter Proper Paid Amount in Rs\n";
                clicked.parent().parent().find('[name="p_amnt"]').css('border-color','red');
            }else if(parseInt(t_amnt) < parseInt(p_amnt)){
                check=false;
                error_msg= error_msg+"* Paid amount cannot be greater than total amount.\n";
                clicked.parent().parent().find('[name="p_amnt"]').css('border-color','red');
                clicked.parent().parent().find('[name="t_amnt"]').css('border-color','red');
            }
        }

        if(dt_amnt == ''){
            check = false;
            error_msg = error_msg+"* Please select a date at which amount is paid.\n";
            clicked.parent().parent().find('[name="dt_amnt"]').css('border-color','red');
        }else if(!dt_amnt.match(/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/)){
            check = false;
            error_msg = error_msg+"* Please enter valid date like 'mm/dd/yyyy'.\n";
            clicked.parent().parent().find('[name="dt_amnt"]').css('border-color','red');
        }

        if(check === false){
			// clicked.parent().parent().parent().prepend('<div style="width: 100%;margin: 10px;">'+error_msg+'</div>');
            // error_msg ="";
            alert(error_msg);
		}

        if(check === true){
            var sp_str = 'sup_id='+sup_id+'&sup_name='+sup_name+'&sup_phone='+sup_phone+'&sup_address='+ sup_address+
            '&p_amnt='+ p_amnt+'&dt_amnt='+ dt_amnt+'&d_amnt='+ d_amnt+'&t_amnt='+ t_amnt+"&userid="+userid+'&oper='+'updateSupplier';
            $.ajax({
                type: "POST",
                data:sp_str,
                url: "includes/controllers/admin_controller.php",
                success: function(result){
                    result = result.split('~');
                    if(result[0] == "OK"){
                        clicked.parent().parent().removeClass('edit');
                        clicked.parent().parent().find('[name="d_amnt"]').val(d_amnt);
                        // clicked.find('[name="btn-row-edit"]').show();
                        $('[name="btn-row-edit"]').show();
                    }else{
                        alert("Please try again, some error occured");
                    }
                }
            });
            return false;
        }
    });

    ////Display Information of supplier, individually.
    $(document).on('click','[name="btn-row-open"]',function(evt){
        evt.preventDefault();
        $('[class="modal"]').modal('show');
        evt =evt;
        clicked = $(evt.target);
        var sp_id = clicked.attr('id');
        $('[name="sup-slctd-id"]').val(sp_id);
        $.ajax({
            type: "POST",
            data:{'sp_id':sp_id,'userid':userid},
            url: "includes/controllers/admin_controller.php?oper=showSupplier",
            success: function(result){
                $('#sup-dtl-show').empty();
                $('#sup-dtl-show').append(result);
            }
        });
        return false;
    });

    ////////////End Supplier section/////////////////////////



    ///////////Updating or removing maker information////////////////
	//Remove a maker
    $(document).on("click",'[name="dlt-btn-mkr"]',function(evt){
        evt.preventDefault();
        evt =evt;
        clicked = $(evt.target);
        if(clicked.prop('tagName')=="I"){
             clicked=clicked.parent();
         }
        var mkrid= clicked.parent().parent().attr('id');

        if (confirm("Are you sure want to delete Maker?")) {       
            $.ajax({
                type: "POST",
                url: "includes/controllers/admin_controller.php?oper=deleteMaker",
                data: "mkrid="+mkrid+"&userid="+userid,
                success: function(result){
                    if(result == "OK"){
                    	clicked.parent().parent().remove();
                    }else{
                    	alert("Please try again, some error occured");
                    }
                }
            });
            return false;
        }
        return false;
    });

    //Updating Maker information.
    $(document).on("click",'.mkr-upd-save',function(evt){
        evt.preventDefault();
        evt=evt;
        clicked= $(evt.target);
        
        if(clicked.prop('tagName')=="I"){
            clicked=clicked.parent();
        }

        var mkr_id = clicked.parent().parent().attr('id');
        var mkr_name =clicked.parent().parent().find('[name="maker_name"]').val();
        var mkr_str = 'mkr_id='+mkr_id+'&mkr_name='+mkr_name+"&userid="+userid+'&oper='+'updateMaker';
        $.ajax({
            type: "POST",
            data:mkr_str,
            url: "includes/controllers/admin_controller.php",
            success: function(result){
            	if(result == "OK"){
                	clicked.parent().parent().removeClass('edit');
                	$('[name="btn-row-edit"]').show();
                }else{
                	alert("Please try again, some error occured");
                }
            }
        });
        return false;
    });
    /////////////////////End Maker section//////////////////////////


    ///////////Updating or removing types information////////////////
	//Remove a type
    $(document).on("click",'[name="dlt-btn-type"]',function(evt){
        evt.preventDefault();
        evt =evt;
        clicked = $(evt.target);
        if(clicked.prop('tagName')=="I"){
             clicked=clicked.parent();
         }
        var typid= clicked.parent().parent().attr('id');

        if (confirm("Are you sure want to delete Type?")) {       
            $.ajax({
                type: "POST",
                url: "includes/controllers/admin_controller.php?oper=deleteType",
                data: "typid="+typid+"&userid="+userid,
                success: function(result){
                    if(result == "OK"){
                    	clicked.parent().parent().remove();
                    }else{
                    	alert("Please try again, some error occured");
                    }
                }
            });
            return false;
        }
        return false;
    });

    //Updating Type information.
    $(document).on("click",'.typ-upd-save',function(evt){
        evt.preventDefault();
        evt=evt;
        clicked= $(evt.target);
        
        if(clicked.prop('tagName')=="I"){
            clicked=clicked.parent();
        }

        var typ_id = clicked.parent().parent().attr('id');
        var typ_name =clicked.parent().parent().find('[name="type_name"]').val();
        var typ_str = 'typ_id='+typ_id+'&typ_name='+typ_name+"&userid="+userid+'&oper='+'updateType';
        $.ajax({
            type: "POST",
            data:typ_str,
            url: "includes/controllers/admin_controller.php",
            success: function(result){
            	if(result == "OK"){
                	clicked.parent().parent().removeClass('edit');
                	$('[name="btn-row-edit"]').show();
                }else{
                	alert("Please try again, some error occured");
                }
            }
        });
        return false;
    });

    ////////////////End  Type information/////////////////////////


    ///////////Updating or removing article information////////////////
	//Remove an article
    $(document).on("click",'[name="dlt-btn-artcl"]',function(evt){
        evt.preventDefault();
        evt =evt;
        clicked = $(evt.target);
        if(clicked.prop('tagName')=="I"){
             clicked=clicked.parent();
         }
        var artclid= clicked.parent().parent().attr('id');

        if (confirm("Are you sure want to delete an Article?")) {       
            $.ajax({
                type: "POST",
                url: "includes/controllers/admin_controller.php?oper=deleteArticle",
                data: "artclid="+artclid+"&userid="+userid,
                success: function(result){
                    if(result == "OK"){
                    	clicked.parent().parent().remove();
                    }else{
                    	alert("Please try again, some error occured");
                    }
                }
            });
            return false;
        }
        return false;
    });

    //Updating Article information.
    $(document).on("click",'.art-upd-save',function(evt){
        evt.preventDefault();
        evt=evt;
        clicked= $(evt.target);
        
        if(clicked.prop('tagName')=="I"){
            clicked=clicked.parent();
        }

        var art_id = clicked.parent().parent().attr('id');
        var art_title =clicked.parent().parent().find('[name="art_title"]').val();
        var art_no =clicked.parent().parent().find('[name="art_no"]').val();
        var art_size =clicked.parent().parent().find('[name="art_size"]').val();
        var art_clr =clicked.parent().parent().find('[name="art_clr"]').val();
        var art_gndr =clicked.parent().parent().find('[name="art_gndr"]').val();
        var art_str = 'art_id='+art_id+'&art_title='+art_title+'&art_no='+art_no+'&art_size='+ art_size+'&art_clr='+
        art_clr+'&art_gndr='+ art_gndr+"&userid="+userid+'&oper='+'updateArticle';
        $.ajax({
            type: "POST",
            data:art_str,
            url: "includes/controllers/admin_controller.php",
            success: function(result){
            	result = result.split('~');
            	if(result[0] == "OK"){
                	clicked.parent().parent().removeClass('edit');
                	clicked.parent().parent().find('[name="art_title"]').val(result[1]);
                	// clicked.find('[name="btn-row-edit"]').show();
                	$('[name="btn-row-edit"]').show();
                }else{
                	alert("Please try again, some error occured");
                }
            }
        });
        return false;
    });

    ///////////////////////End Article Information///////////////////////



	function loadSuppliers(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getSuppliers",
			dataType:'json',
            data: {'userid':userid},
	        // async: false,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Supplier data may not be availible.");
				}else{
                    var d ="";
					var suppliers = $('#table_body_sup');
		            suppliers.empty();
		            for (var i = 0; i < data.length; i++) {
                        if(!data[i].date_paid){
                            var d = data[i].date_paid;
                        }else{
                            var date = new Date(data[i].date_paid * 1000);
                            var day = date.getDate();
                            var month = date.getMonth();
                            var year = date.getFullYear();
                            var d = (month+1)+"/"+day+"/"+year;
                        }

		            	suppliers.append('<tr id="'+ data[i].sup_id	+'" class="editable deactivate">'+''
		            		+'<td style="width:20%;"><button name="dlt-btn-sup" class="btn btn-danger"><i class="fa fa-trash"></i></button>'+''
		            		+'<button name="btn-row-edit" style="margin-left: 3px;" class="btn bg-purple"><i class="fa fa-edit"></i></button>'+''
		            		+'<button id="'+data[i].sup_id+'" name="btn-row-save" class="btn bg-green sup-upd-save" style="margin-left: 3px;"><i id="btn-row-save-i" class="fa fa-save"></i></button>'+''
                            +'<button id="'+data[i].sup_id+'" name="btn-row-open" style="margin-left: 3px;" class="btn bg-blue">Show</button></td>'+''
		            		+'<td><input name="sup_name" value="'+data[i].sup_name+ '"/></td>'+ '' 
		            		 +'<td><input name="sup_phone" value="'+data[i].sup_phone+'" /></td>'+''
		            		 +'<td><input name="sup_address" value="'+data[i].sup_address+ '"/></td>'+''
                             +'<td><input name="p_amnt" value="'+data[i].amount_paid+'" /></td>'+''
                             +'<td><input name="dt_amnt" value="'+d+'" /></td>'+''
                             +'<td><input name="d_amnt" value="'+data[i].amount_due+'" readonly/></td>'+''
                             +'<td><input name="t_amnt" value="'+data[i].amount_total+'" /></td></tr>');
		            	// suppliers.append('<td style="">');
		            	// suppliers.append('<button name="btn-user-deactive" class="btn btn-danger">Deactivate</button>');
		            	// suppliers.append('<button name="btn-user-active" class="btn bg-green">Activate</button>');
		            	// suppliers.append('<button name="btn-row-edit" style="margin-left: 3px;" class="btn bg-purple"><i class="fa fa-edit"></i></button>');		            	
		            	// suppliers.append('<button id="'+ data[i].sup_id +'" name="btn-row-save" class="btn bg-green userSave" style="margin-left: 3px;"><i id="btn-row-save-i" class="fa fa-save"></i></button>');
		            	// suppliers.append('</td>');		            	
		            	// suppliers.append('<td><input name="sup_name" value="'+ data[i].sup_name+ '/></td>');		            	
		            	// suppliers.append('<td><input name="sup_address" value="'+ data[i].sup_address+ '/></td>');		            	
		            	// suppliers.append('<td><input name="sup_phone" value="'+ data[i].sup_phone+ '/></td>');		            	
		            	// suppliers.append('</tr>');
		            }
                    eval(`$("#ed-supplier-dtl").DataTable();`);
				}	
			}
		});
		return false;
	}

	function loadMakers(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getMakers",
			dataType:'json',
            data: {'userid':userid},
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Makers data may not be availible.");
				}else{
					var makers = $('#table_body_maker');
		            makers.empty();
		            for (var i = 0; i < data.length; i++) {
		            	makers.append('<tr id="'+ data[i].maker_id	+'" class="editable deactivate">'+''
		            		+'<td style=""><button name="dlt-btn-mkr" class="btn btn-danger"><i class="fa fa-trash"></i></button>'+''
		            		+'<button name="btn-row-edit" style="margin-left: 3px;" class="btn bg-purple"><i class="fa fa-edit"></i></button>'+''
		            		+'<button id="'+data[i].maker_id+'" name="btn-row-save" class="btn bg-green mkr-upd-save" style="margin-left: 3px;"><i id="btn-row-save-i" class="fa fa-save"></i></button></td>'+''
		            		+'<td><input name="maker_name" value="'+data[i].maker_name+ '"/></td>');
		            }
				}
                eval(`$("#ed-maker-dtl").DataTable();`);

			}
		});
		return false;
	}

	function loadTypes(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getTypes",
			dataType:'json',
            data: {'userid':userid},
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Types data may not be availible.");
				}else{
					var types = $('#table_body_type');
		            types.empty();
		            for (var i = 0; i < data.length; i++) {
		            	types.append('<tr id="'+ data[i].type_id	+'" class="editable deactivate">'+''
		            		+'<td style=""><button name="dlt-btn-type" class="btn btn-danger"><i class="fa fa-trash"></i></button>'+''
		            		+'<button name="btn-row-edit" style="margin-left: 3px;" class="btn bg-purple"><i class="fa fa-edit"></i></button>'+''
		            		+'<button id="'+data[i].type_id+'" name="btn-row-save" class="btn bg-green typ-upd-save" style="margin-left: 3px;"><i id="btn-row-save-i" class="fa fa-save"></i></button></td>'+''
		            		+'<td><input name="type_name" value="'+data[i].type_name+ '"/></td>');
		            }
				}
                eval(`$("#ed-type-dtl").DataTable();`);	
			}
		});
		return false;
	}

	function loadArticles(){
    	$.ajax({
			type: "GET",
			url: "includes/controllers/inventory_controller.php?oper=getArticles",
            data: {'userid':userid},
			dataType:'json',
	        // async: true,
			success: function(data){                                       
				if(data == 'Error'){
					alert("Articles data may not be availible.");
				}else{
					var articles = $('#table_body_artcl');
		            articles.empty();
		            for (var i = 0; i < data.length; i++) {
		            	articles.append('<tr id="'+ data[i].article_id	+'" class="editable deactivate">'+''
		            		+'<td style="width:10%;"><button name="dlt-btn-artcl" class="btn btn-danger"><i class="fa fa-trash"></i></button>'+''
		            		+'<button name="btn-row-edit" style="margin-left: 3px;" class="btn bg-purple"><i class="fa fa-edit"></i></button>'+''
		            		+'<button id="'+data[i].article_id+'" name="btn-row-save" class="btn bg-green art-upd-save" style="margin-left: 3px;"><i id="btn-row-save-i" class="fa fa-save"></i></button></td>'+''
		            		+'<td><input name="art_title" readonly value="'+data[i].article_title+ '"/></td>'+ '' 
		            		 +'<td><input name="art_no" value="'+data[i].article_no+'" /></td>'+''
		            		 +'<td><input name="art_size" value="'+data[i].size_no+ '" /></td>'+''
		            		 +'<td><input name="art_clr" value="'+data[i].color_name+ '" /></td>'+''
		            		 +'<td><input name="art_gndr" value="'+data[i].gender_name+ '" /></td></tr>');
		            }
				}
                eval(`$("#ed-article-dtl").DataTable();`);
			}
		});
		return false;
	}

});