$(document).ready(function(){
	$('#sales_detail_error_message').hide();
	$('#cart-box').hide();
	$('#cart-Btn').prop("disabled",true);
	$('#expense_error_message').hide();
	$('#sales-error-msg').hide();

	///Search for creating a sale//////
	$('#search-id-btn').click(function(evt){
		evt.preventDefault();
		var q = $('#serachq').val();

		$.ajax({
			type: "POST",
			data:{'q':q,'userid':userid},
			url: "includes/controllers/sales_controller.php?oper=getSearchResults",
	        // async: true,
			success: function(data){                                       
				if(data === 'Error'){
					$('#sales-error-msg').show();
					$('#sales-error-msg').html("No data match with article id");
					$('#search-results').html('');
					$('#cart-box').hide();
				}else{
					$('#search-results').html(data);
					$('#cart-box').show();
					$('#sales-error-msg').hide();					
				}	
			}
		});
		return false;

	});

	/////////Search for replaing/return an item
	$('#search-id-rp-btn').click(function(evt){
		evt.preventDefault();
		var qrp = $('#serachq-rp').val();

		$.ajax({
			type: "POST",
			data:{'q':qrp,'userid':userid},
			url: "includes/controllers/sales_controller.php?oper=getReplaceSearchResults",
	        async: true,
			success: function(data){                                       
				if(data == 'Error'){
					$('#sales-error-msg').show();
					$('#sales-error-msg').html("No data match with article id");
					$('#search-results').html('');
				}else{
					$('#search-results').html(data);
					$('#sales-error-msg').hide();					
				}	
			}
		});
		return false;
	});

	//////Cart Portion Start/////////////////
	$(document).on('click','[name="add-to-cart-Btn"]',function(evt){
		evt.preventDefault();
		evt=evt;
        clicked= $(evt.target);
        var check = true;
        var error_msg ="";
		$('#order_error_message').hide();


        var id = clicked.attr('id');
        var sales_price = clicked.parent().find('#sales_price').val();
        var cartBtnval = $('#cart-Btn').attr('list-product');
        cartBtnval += id+"~";
        var cartBtnprice = $('#cart-Btn').attr('list-price');
        cartBtnprice += sales_price +"~";
        var countItems = $('#countItems').html();
        countItems = parseInt(countItems);
        countItems++;

        if(sales_price.length<=0){
			 check=false;
			 error_msg= error_msg+"* Enter Sales Price to be Added<br/>";
			 $('#sales_price').css('border-color','red');
		}

		if(sales_price.length >0){                     
			if(!($.isNumeric(sales_price))){
				check=false;
				error_msg= error_msg+"* Please Enter A Valid Price<br/>";
				$('#sales_price').css('border-color','red');
			}
		}
		if(check === false){
			$('#order_error_message').html(error_msg);
			$('#order_error_message').show();
		}

        if(countItems > 0){
        	if(check === true){
        		$('#countItems').html(countItems);
		        $('#cart-Btn').attr('list-product',cartBtnval);
		        $('#cart-Btn').attr('list-price',cartBtnprice);
				$('#cart-Btn').prop("disabled",false);
				clicked.addClass('btn-default').removeClass('btn-info');
				clicked.html("Product is Added To Cart");
				clicked.prop("disabled",true);
				clicked.parent().find('#sales_price').val('');
				clicked.parent().find('#sales_price').prop("disabled",true);
				clicked.parent().find('#sales_price').attr("placeholder", "Sales Price '"+ sales_price+"-Rs' is added.");
				clicked.parent().find('#sales_price').fadeOut(6000);
				// var encoded = encodeURIComponent(cartBtnval);
		        var url = 'includes/views/sales-invoice.php?solditems='+cartBtnval+'&soldprice='+cartBtnprice;
				$('#cart-Btn').children().attr('href',url);
        	}
        }
	});

	$(document).on('click','#cart-Btn',function(evt){
		evt.preventDefault();
        var productids = $('#cart-Btn').attr('list-product');
        var productPrices = $('#cart-Btn').attr('list-price');
        var url = $('#cart-Btn').children().attr('href');

	    $.ajax({
			type: "POST",
			data:{'productids':productids,'productPrices':productPrices,'userid':userid},
			url: "includes/controllers/sales_controller.php?oper=solditemsStatus",
			success: function(data){                                       
				if(data == 'Error'){
					alert("Status not updated to sold.");
				}else{
					window.open(
					  url,
					  '_blank' // <- This is what makes it open in a new window.
					);
					$('#cart-Btn').attr('list-product','');
			        $('#cart-Btn').attr('list-price','');
			        $('#countItems').html('0');
				}	
			}
		});
		return false;        
	});
	/////////////////Cart Portion ended/////////////////


	//////////////extra Expenses Portion Start////////	
	$(document).on('click','#submit_expense_detail',function(evt){
		evt.preventDefault();
        var check = true;
        var error_msg ="";

        $('#submit_expense_detail').prop("disabled",true);

		var amnt = $('#expense-amount').val();
		var desc = $('#expense-desc').val();
		// var ex_date = $('#expense-date').val();
		$('#expense-amount').css('border-color','#3c8dbc');
		$('#expense-desc').css('border-color','#3c8dbc');
		// $('#expense-date').css('border-color','#3c8dbc');

		if(amnt.length<=0){
			check=false;
			error_msg= error_msg+"* Enter Expense Amount to be Added<br/>";
			$('#expense-amount').css('border-color','red');
		}

		if(amnt.length >0){                     
			if(!($.isNumeric(amnt))){
				check=false;
				error_msg= error_msg+"* Please Enter Proper Amount<br/>";
				$('#expense-amount').css('border-color','red');
			}
		}

		if(desc == ''){
			check = false;
			error_msg = error_msg+"* Please enter expense description.<br>";
			$('#expense-desc').css('border-color','red');
		}

		if(check === false){
			$('#expense_error_message').html(error_msg);
			$('#expense_error_message').show();
	        $('#submit_expense_detail').prop("disabled",false);

		}

		if(check === true){
			$('#expense_error_message').hide();
	    	var expenseInfo = "amnt="+amnt+"&desc="+desc+"&userid="+userid+"&action="+"addExpense";
	    	$.ajax({
				type: "POST",
				url: "includes/controllers/sales_controller.php",
				data: expenseInfo,
				success: function(result){
					if(result === "OK"){
			        	$('#submit_expense_detail').prop("disabled",false);
				        $('#expense-amount').val('');
						$('#expense-desc').val('');
						$('#expense-date').val('');
						alert("Expense recorded succesfully.");
					}else{
			        	$('#submit_expense_detail').prop("disabled",false);
						alert("Some error occured, please try again.");
					}	
				}
			});
			return false;
		}
	});
	///////Extra expense portion ended////////////////////////


	/////// To replace/return an item///
	$(document).on('click','[name="replace-Btn"]',function(evt){
		evt.preventDefault();
		evt=evt;
        clicked= $(evt.target);
        var p_id = clicked.attr('id');

        if (confirm("Are you sure you want to return this sale?")) {
			$.ajax({
				type: "POST",
				data:{'p_id':p_id,'userid':userid},
				url: "includes/controllers/sales_controller.php?oper=returnSales",
				success: function(result){
					if(result === "OK"){
						alert("Sales has been added back to the inventory succesfully!");
						clicked.prop("disabled",true);
					}else{
						alert("Some error occured, please try again.");
					}	
				}
			});
			return false;
		}
		return false;
	});
});