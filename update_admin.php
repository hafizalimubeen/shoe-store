<?php include('includes/views/session.php');?>
<!DOCTYPE html>
<html>
  <!--head-->
  <?php include('includes/html-parts/head.php');?>
  <!--head-->
  <body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
    <!-- header -->
      <?php include('includes/views/header.php');?>
      <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>        
              <?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?>
              </p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span> 
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">Admin Panel Menu </li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="admin.php">
                <i class="fa fa-folder"></i> <span>Admin Panel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a></li>
            <li><a href="update_admin.php">
                <i class="fa fa-circle"></i>
                <span>Update Admin Panel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a></li>
          </ul><!-- /.sidebar-menu --> 
        </section> 
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
              <a id="ed-sup" href="#"><i class="fa fa-briefcase" style="margin-left: 40px;color:#57bddb;"></i>Update Supplier</a>
              <a id="ed-maker" href="#"><i class="fa fa-gears" style="margin-left: 40px;color:#57bddb;"></i>Update Maker</a> 
              <a id="ed-typ" href="#"><i class="fa fa-industry" style="margin-left: 40px;color:#57bddb;"></i>Update Type</a>
              <a id="ed-artcl" href="#"><i class="fa fa-hand-lizard-o" style="margin-left: 40px;color:#57bddb;"></i>Update Article</a>
<!--               <a id="clr" href="#"><i class="fa fa-circle" style="margin-left: 40px;color:#57bddb;"></i>New Color</a>
              <a id="siz" href="#"><i class="fa fa-hand-grab-o" style="margin-left: 40px;color:#57bddb;"></i>Add New Size</a> -->       
          </h4>                     
        </section> 

        <div class=" col-md-12" style="background: white;padding-top: 30px;margin-top:25px;">

          <!-- Supplier Information -->
          <div class="col-sm-12"> 
              <table class="table table-bordered table-hover dataTable display" id="ed-supplier-dtl">
                <thead>
                    <tr>
                        <th></th>
                        <th>Supplier Name</th>
                        <th>Phone No.</th>
                        <th>Address</th>
                        <th>Amount Paid</th>
                        <th>Paid Date</th>
                        <th>Amount Due</th>
                        <th>Total Amount</th>
                    </tr>
                </thead>
                <tbody id="table_body_sup">
                </tbody>
                <tfoot>
                </tfoot>
            </table>
          </div>

          <!-- Article information -->
          <div class="col-sm-12"> 
            <table class="table table-bordered table-hover dataTable display" id="ed-article-dtl">
              <thead>
                  <tr>
                      <th></th><th>Title of article</th><th>Article No.</th><th>Shoe Size</th><th>Shoe Color</th>
                      <th>Shoe Wear</th>
                  </tr>
              </thead>
              <tbody id="table_body_artcl">
                
              </tbody>
              <tfoot>
              </tfoot>
          </table>
        </div>


        <!-- Maker information -->
        <div class="col-sm-8"> 
          <table class="table table-bordered table-hover dataTable display" id="ed-maker-dtl">
            <thead>
                <tr>
                    <th></th><th>Maker Name</th>
                </tr>
            </thead>
            <tbody id="table_body_maker">
            </tbody>
            <tfoot>
            </tfoot>
          </table>
        </div>

        <!-- Type information -->
        <div class="col-sm-8"> 
          <table class="table table-bordered table-hover dataTable display" id="ed-type-dtl">
            <thead>
                <tr>
                    <th></th><th>Type Name</th>
                </tr>
            </thead>
            <tbody id="table_body_type">
            </tbody>
            <tfoot>
            </tfoot>
          </table>
        </div>                   
</div>                                                       

      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include('includes/views/footer.php');?>

      <!-- Control Sidebar -->
      <?php include('includes/views/rightbar.php');?>
      
    </div><!-- ./wrapper -->
    <div class="modal">
      <div class="modal-dialog" style="top: 150px;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <!-- <h4 class="text-muted text-center">Supplier Detail</h4> -->
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="box box-solid">
                  <div class="box-header with-border">
                    <!-- <i class="fa fa-text-width"></i> -->
                    <h3 class="box-title">Supplier Detail</h3>
                  </div><!-- /.box-header -->
                  <div id="sup-dtl-show" class="box-body">
                    
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div>
            </div>
            <div>
              <input type="hidden" name="sup-slctd-id">
            </div>
            <!-- <div class="row">
              <div class="col-lg-6 col-xs-6">
                <div class="form-group has-feedback">
                  <input id="video-title" type="textarea" name="url" class="form-control" placeholder="enter video URL">
                  <span class="glyphicon glyphicon-share-alt form-control-feedback"></span>
                </div>
              </div>
              <div class="col-lg-6 col-xs-6">
                <div class="form-group has-feedback">
                  <input id="video-title" type="text" name="title" class="form-control" placeholder="enter video title">
                  <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                </div>
              </div>
            </div> -->

<!--             <div class="row">
              <div class="col-xs-12">
                <div class="error" name="error_msgs"></div>
              </div>
            </div>   -->          
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
     
    <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
  </body>
</html>