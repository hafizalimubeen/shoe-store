<?php include('includes/views/session.php');?>
<!DOCTYPE html>
<html>
  <!--head-->
  <?php include('includes/html-parts/head.php');?>
  <!--head -->
  <body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
    <!-- header -->
      <?php include('includes/views/header.php');?>
            <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>        
              <?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?>
              </p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span> 
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->

          </ul><!-- /.sidebar-menu --> 
          <ul class="sidebar-menu">
            <li class=""><a href="sales.php"><i class="fa fa-folder"></i>Create Sales<i class="fa fa-angle-left pull-right"></i></a> </li>
            <li class="active"><a href="expense.php"><i class="fa fa-folder"></i>Extra Expense<i class="fa fa-angle-left pull-right"></i></a></li>
            <li class=""><a href="return.php"><i class="fa fa-folder"></i>Return Sales<i class="fa fa-angle-left pull-right"></i></a></li>

          </ul><!-- /.sidebar-menu --> 
        </section> 
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>            
              <i class="ion ion-bag" style="margin-left: 50px;color:#57bddb;"></i>Other Expenses
          </h1>                     
        </section> 


      <div class=" col-md-12" style="background: white;padding-top: 30px;margin-top:25px;">
              
              <div class="col-md-offset-3 col-md-6">
                <!--  -->
                <!-- Add Extra Expenses   -->
                <!--  -->
                <div id="purchase_order" class="box box-primary">                  
                  <div class="box-header with-border">
                    <h3 class="box-title"><b>Add your other expenses here</b></h3>
                  </div>
                  <div id="expense_detail">
                    <div class="box-body">
                          <div class="form-group">                        
                              <label for="username">Enter Expense Amount</label>
                              <input type="text" class="form-control" id="expense-amount" placeholder="enter amount in Rupees"/> -Rs          
                          </div>

                          <div class="form-group">                        
                              <label for="username">Expense Description/Detail</label>
                              <input type="text" class="form-control" id="expense-desc" placeholder="nature of expense"/>          
                          </div>
                        
<!--                           <div class="form-group">
                              <label for="exampleInputPassword1">Date of Expense Occur</label>                      
                              <input type="date" class="form-control" id="expense-date" />
                          </div>  -->                                                    
                         
                    </div>
                      
                    <div id="expense_error_message" class="validatr-message" style="color: rgb(240, 68, 77); border: 1px solid rgb(228, 166, 175); padding: 2px 6px; border-radius: 0px; position: relative; left: 19px; top: 0px; background-color: rgb(255, 203, 203);">
                     </div>  
                    <div class="box-footer">
                      <button id="submit_expense_detail" class="btn btn-primary">Add</button>
                    </div>
                  </div>
                </div>
                <!--  -->
                <!-- Expense DETAILS end -->
                <!-- -->

              </div>
      </div>                                                         




      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include('includes/views/footer.php');?>

      <!-- Control Sidebar -->
      <?php include('includes/views/rightbar.php');?>
      
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
  </body>
</html>