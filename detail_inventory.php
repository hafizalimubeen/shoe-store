<?php include('includes/views/session.php');?>
<!DOCTYPE html>
<html>
  <!--head-->
  <?php include('includes/html-parts/head.php');?>
  <!--head-->
  <body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
    <!-- header -->
      <?php include('includes/views/header.php');?>
      <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>        
              <?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?>
              </p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span> 
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->

          </ul><!-- /.sidebar-menu -->
          <ul class="sidebar-menu">
            <li class=""><a href="add_inventory.php"><i class="fa fa-folder"></i>Add Inventory<i class="fa fa-angle-left pull-right"></i></a></li>
            <li class="active"><a href="detail_inventory.php"><i class="fa fa-folder"></i>Purchase Order<i class="fa fa-angle-left pull-right"></i></a></li>
          </ul><!-- /.sidebar-menu -->  
        </section> 
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>            
              <i class="ion ion-bag" style="margin-left: 50px;color:#57bddb;"></i>Purchase Order and Detail
          </h1>                     
        </section> 


      <div class=" col-md-12" style="background: white;padding-top: 30px;margin-top:25px;">
        <div class="col-sm-12">
            
              <div id="purchase_order" class="box box-primary">                  
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Search purchase order detail from the availible list of suppliers</b></h3>
                </div>
                <div id="order_detail" class="form-horizontal">
                  <div class="box-body">
                      
                        <div class="form-group">                        
                            <label class="col-sm-4 control-label" for="username">Select a Supplier</label>
                            <div class="col-sm-4">
                              <select class="form-control" id="supplier-name">
                                  <option value="" disabled selected>Please Select a Supplier</option>
                              </select> 
                            </div>                     
                        </div>
                      
                                                   
                      
                        <div class="form-group">
                            <label style="color:#57bddb">Your are Logged in As: </label>
                            <label><?php if(isset($_SESSION['usertype'])){ echo " ".$_SESSION['usertype'];}?></label>
                        </div>   
                       
                  </div>
                   <!-- Supplier Information -->
                 <div id="product-dtl-div">
                   <div class="col-sm-12"> 
                      <table class="table table-bordered table-hover dataTable display" id="ed-product-dtl">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Product Title</th>
                                <th>Purchase Price</th>
                                <th>Purchase Time</th>
                                <th>Extra Title</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody id="table_body_product">
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                  </div>
                 </div>
                </div>
              </div>
            </div> 
        </div>                                                         




      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include('includes/views/footer.php');?>

      <!-- Control Sidebar -->
      <?php include('includes/views/rightbar.php');?>
      
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
  </body>
</html>