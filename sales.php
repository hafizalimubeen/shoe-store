<?php include('includes/views/session.php');?>
<!DOCTYPE html>
<html>
  <!--head-->
  <?php include('includes/html-parts/head.php');?>
  <!--head-->
  <body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
    <!-- header -->
      <?php include('includes/views/header.php');?>
      <!-- Left side column. contains the logo and sidebar -->
                 <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>        
              <?php if(isset($_SESSION['fullname'])){ echo $fullname;} ?>
              </p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span> 
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->

          </ul><!-- /.sidebar-menu --> 
          <ul class="sidebar-menu">
            <li class="active"><a href="sales.php"><i class="fa fa-folder"></i>Create Sales<i class="fa fa-angle-left pull-right"></i></a> </li>
            <li class=""><a href="expense.php"><i class="fa fa-folder"></i>Extra Expense<i class="fa fa-angle-left pull-right"></i></a></li>
            <li class=""><a href="return.php"><i class="fa fa-folder"></i>Return Sales<i class="fa fa-angle-left pull-right"></i></a></li>
          </ul><!-- /.sidebar-menu --> 
        </section> 
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>            
              <i class="ion ion-bag" style="margin-left: 50px;color:#57bddb;"></i>Sales Management
          </h1>                     
        </section> 


      <div class=" col-md-12" style="background: white;padding-top: 30px;margin-top:25px;">
      <div class="col-md-offset-2 col-md-6">
          <div id="search-sales-box" class="input-group margin">
            <input type="text" class="form-control" id="serachq" placeholder="enter shoes Article Number">
            <span class="input-group-btn">
              <button id="search-id-btn" class="btn btn-info btn-flat" type="button">Search</button>
            </span>
          </div>
          <small>Note: Only search with article number. . .</small>
          <p id="sales-error-msg" style="color: red; font-weight: bold;"></p>
      </div>
      <div class="col-sm-offset-7 col-md-3" id="cart-box" style="position:fixed;width: 20%;">
        <label id="product-count-label" >Items Added to cart: <b id="countItems">0</b></label>
        <button id="cart-Btn" list-product="" list-price="" class="btn btn-block btn-primary btn-sm"><a href="" target="_blank" style="color:black;">Click Here To Check Out</a></button>
      </div>
      <div id="search-results">
        
      </div>
<!--         <div class="col-md-offset-3 col-md-4">
              <div id="adding_sales" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><b>Please Add Sales Details</b></h3><br/>                  
                </div>
                <div id="order">
                  <div class="box-body">                                              
                      
                        <div id="maker-1" class="form-group">                        
                            <label for="sales_maker">Select a Maker</label>
                            <select class="form-control" id="sales_maker">
                                <option value="" disabled selected>Please Select a Maker</option>
                            </select>                      
                        </div>
                        
                        <div id="type-2" class="form-group">                        
                            <label for="sales_detail_type">Select a Type</label>
                            <select class="form-control" id="sales_detail_type">
                                <option value="" disabled selected>Please Select a Type</option>
                            </select>                      
                        </div>

                        <div id="gender-3" class="form-group">                        
                            <label for="sales_detail_gender">Select a Gender</label>
                            <select class="form-control" id="sales_detail_gender">
                                <option value="" disabled selected>Please Select a Gender</option>
                            </select>                      
                        </div>

                        <div id="article-4" class="form-group">                        
                            <label for="sales_detail_article">Select an Article</label>
                            <select class="form-control" id="sales_detail_article">
                                <option value="" disabled selected>Please Select an Article</option>
                            </select>                      
                        </div>

                        <div id="size-5" class="form-group">                        
                            <label for="sales_detail_article">Select a Size</label>
                            <select class="form-control" id="sales_detail_article">
                                <option value="" disabled selected>Please Select a Size</option>
                            </select>                      
                        </div>
                        
                        <div id="color-6" class="form-group">                        
                            <label for="sales_detail_color">Select a Color</label>
                            <select class="form-control" id="sales_detail_color">
                                <option value="" disabled selected>Please Select a Color</option>
                            </select>                      
                        </div>
                        
                        <div id="another-7" class="form-group">
                            <label for="another_name">Enter extra title</label>                      
                            <input type="text" class="form-control" id="another_name" placeholder="Enter another name if any"/>
                        </div>
                        
                        <div id="sales-9" class="form-group">
                            <label for="sales_price">Enter Sales Price</label>                      
                            <input type="text" class="form-control" id="Sales_price" placeholder="Enter Sales Price for Shoes"/>
                        </div>

                        <div class="form-group">
                            <label for="sales_quantity">Enter Shoes Quantity</label>                      
                            <input type="number" min="1" max="100" class="form-control" id="sales_quantity" placeholder="Enter Quantity of Shoes"/>
                        </div>
                                
                  </div>
                    
                  <div id="sales_detail_error_message" class="validatr-message" style="color: rgb(240, 68, 77); border: 1px solid rgb(228, 166, 175); padding: 2px 6px; border-radius: 0px; position: relative; left: 19px; top: 0px; background-color: rgb(255, 203, 203);">
                   </div>  
                  <div id="footer10" class="box-footer">
                    <button id="submit_sold_product_detail" class="btn btn-primary">Next</button>
                    <button id="done_order" class="btn btn-primary" style="margin-left:50px">Finish</button>
                  </div>
                </div>
              </div>                 
            </div>  -->
        </div>                                                         




      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include('includes/views/footer.php');?>

      <!-- Control Sidebar -->
      <?php include('includes/views/rightbar.php');?>
      
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
        <!-- <script>
             var map;
             function initMap(){                
                    map = new google.maps.Map(document.getElementById('world-map'), {
                    center: {lat: 30.774505540304002, lng: 72.43481934070587},
                    zoom: 16                   
                    });        
                   var marker=new google.maps.Marker({
                        position:{lat: 30.774505540304002,lng: 72.43481934070587},
                        map:map,
                   });              
             }        
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsogWjl6FoMi73yLHQJcPe5E7yDf7xBng&callback=initMap"
    async defer></script> -->
  </body>
</html>