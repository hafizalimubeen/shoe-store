<?php include('includes/views/session.php');?>
<!DOCTYPE html>
<html>
  <!--head-->
  <?php include('includes/html-parts/head.php');?>
  <!--head-->
  <body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">
    <!-- header -->
      <?php include('includes/views/header.php');?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php include('includes/views/sidebar.php');?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Home 
            <small>Welcome!</small>
          </h1>
          <ol class="breadcrumb">
            <li class="active">Home</li>
          </ol>
        </section>

        <!-- Main content -->
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <!-----  --->
          <!-- Small boxes (Stat box) -->
          <!-----  --->
          <!-- Small boxes (Stat box) -->
          <!-----  --->
          <!-- Small boxes (Stat box) -->
          <!-----  --->
          <div class="row">
                        
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>Admin</h3>
                  <p>Panel</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="admin.php" class="small-box-footer">BEGIN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>MANAGE</h3>
                  <p>INVENTORY</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="add_inventory.php" class="small-box-footer">BEGIN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>MANAGE</h3>
                  <p>SALES</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="sales.php" class="small-box-footer">BEGIN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>GENERATE</h3>
                  <p>Reports</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="reports_home.php" class="small-box-footer">GENERATE <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          
          <!-----Menu Items ---->
          <!-----Menu Items ---->
          <!-----Menu Items ---->
          <!-----Menu Items ---->
          <!-----Menu Items ---->
          <!-----Menu Items ---->
          <!----- ---->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-7 connectedSortable">
              <!-- Custom tabs (Charts with tabs)-->
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  
                  <li class="pull-left header"><i class="fa fa-inbox"></i> Shoe Store</li>                  
                </ul>
                <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <img src="dist/img/photo1.jpg" style="position: relative; height: 300px;width:600px;">
                </div>
              </div><!-- /.nav-tabs-custom -->
              
              <div class="box box-success">
                <div class="box-header">
                    <!-- Calendar -->
              <div class="box box-solid bg-green-gradient">
                <div class="box-header">
                  <i class="fa fa-calendar"></i>
                  <h3 class="box-title">Calendar</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <!-- button with a dropdown -->                    
                    <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <!--The calendar -->
                  <div id="calendar" style="width: 100%"></div>
                </div><!-- /.box-body -->                
              </div><!-- /.box -->

                    <!--- Calendar ENds ---->
                  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                  </div>
                </div>
                                  
              </div><!-- /.box (chat box) -->
                              

            </section><!-- /.Left col -->

            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable">

              <!-- Map box -->
              <div class="box box-solid bg-light-blue-gradient">
                <div class="box-header">
                  <!-- tools box -->
                  <div class="pull-right box-tools">                    
                    <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                  </div><!-- /. tools -->

                  <i class="fa fa-map-marker"></i>
                  <h3 class="box-title">
                    Directions
                  </h3>
                </div>
                <div class="box-body">
                  <div id="world-map" style="height: 250px; width: 100%;"></div>
                </div><!-- /.box-body-->
                <div class="box-footer no-border">
                  <div class="row">
                    <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  </div><!-- /.row -->
                </div>
              </div>
              <!-- /.box -->

              <!-- solid sales graph -->
              <div class="box box-solid bg-teal-gradient">
                <div class="box-header">
                  <i class="fa fa-th"></i>
                  <h3 class="box-title">Sales Graph</h3>
                  <div class="box-tools pull-right">
                    <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body border-radius-none">
                  <div class="chart" id="line-chart" style="height: 250px;"></div>
                </div>
                <div class="box-footer no-border">                                                        
                </div>
              </div>
            </section>
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <?php include('includes/views/footer.php');?>

      <!-- Control Sidebar -->
      <?php include('includes/views/rightbar.php');?>
      
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <?php include('includes/html-parts/foot.php');?>
    <script>
        var map;
        function initMap(){                
              map = new google.maps.Map(document.getElementById('world-map'), {
              center: {lat:  31.1467, lng: 72.6876},
              zoom: 16                   
              });        
              var marker=new google.maps.Marker({
                  position:{lat: 31.1467,lng: 72.6876},
                  map:map,
              });              
        }       
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsogWjl6FoMi73yLHQJcPe5E7yDf7xBng&callback=initMap"
    async defer></script> 
  </body>
</html>
